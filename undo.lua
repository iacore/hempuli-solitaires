doundo = true
undobuffer = {}

function initundo()
	undobuffer = {}
	doundo = true
end

function newundo()
	if doundo and ((#undobuffer == 0) or (#undobuffer[1] > 0)) then
		table.insert(undobuffer, 1, {})
	end
end

function addundo(data)
	if doundo then
		local curr = undobuffer[1] or {}
		table.insert(curr, 1, {})
		
		-- alert(tostring(data[1]) .. ", " .. tostring(data[3]))
		
		for i,v in ipairs(data) do
			table.insert(curr[1], v)
		end
	end
end

function undo()
	if (#undobuffer > 0) then
		local curr = undobuffer[1]
		
		for i,v in ipairs(curr) do
			local n = v[1]
			
			-- alert(tostring(n) .. ", " .. tostring(v[3]))
			
			if (n == "move") then
				local unit = getunit(v[2])
				unit.x = v[3]
				unit.y = v[4]
				unit.dir = v[5]
				unit.poscorrect = 1
			elseif (n == "create") then
				delete(v[2],true)
				adddecor(v[2],v[3],v[4],v[11],v[9],v[10])
			elseif (n == "delete") then
				local unit = create(v[3],v[4],v[5],v[2],v[6],v[7],v[8],true,v[9])
			end
		end
		
		table.remove(undobuffer, 1)
	end
end