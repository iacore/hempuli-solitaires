-- v.0.1.2
menusys =
{
	init = function(default)
		MENU = {{}}
		MENU.name = ""
		
		if (default ~= nil) then
			menusys.change(default)
		end
	end,
	
	change = function(newmenu)
		local current = MENU[1]
		local oldmenu = current.name or ""
		
		if (#oldmenu > 0) then
			if (current.leave ~= nil) then
				current.leave(newmenu)
			end
		end
		
		if (menulist[newmenu] ~= nil) then
			MENU[1] = menulist[newmenu]
			MENU[1].name = newmenu
			
			if (MENU[1].enter ~= nil) then
				MENU[1].enter()
			end
			
			if (MENU[1].input ~= nil) then
				inputsys.set(MENU[1].input)
			end
		else
			MENU[1] = {}
			MENU[1].name = newmenu
			inputsys.set()
		end
		
		MENU.name = MENU[1].name
	end,
	
	sub = function(newmenu)
		local current = MENU[1]
		local oldmenu = current.name or ""
		
		if (#oldmenu > 0) then
			if (current.leave_submenu ~= nil) then
				current.leave_submenu(newmenu)
			end
		end
		
		if (menulist[newmenu] ~= nil) then
			table.insert(MENU, 1, menulist[newmenu])
			MENU[1].name = newmenu
			
			if (MENU[1].enter ~= nil) then
				MENU[1].enter()
			end
			
			if (MENU[1].input ~= nil) then
				inputsys.set(MENU[1].input)
			end
		else
			table.insert(MENU, 1, menulist[newmenu])
			MENU[1] = {}
			MENU[1].name = newmenu
			inputsys.set()
		end
		
		MENU.name = MENU[1].name
	end,
	
	close = function()
		local current = MENU[1]
		local oldmenu = current.name or ""
		
		if (#MENU > 1) then
			local new = MENU[2]
			local newmenu = new.name
			
			if (#oldmenu > 0) then
				if (current.leave ~= nil) then
					current.leave(newmenu)
				end
			end
			
			table.remove(MENU, 1)
			
			if (MENU[1].enter_submenu ~= nil) then
				MENU[1].enter_submenu()
			end
			
			if (MENU[1].input ~= nil) then
				inputsys.set(MENU[1].input)
			else
				inputsys.set()
			end
		end
		
		MENU.name = MENU[1].name
	end,
	
	draw = function()
		local current = MENU[1] or {}
		
		if (current.draw ~= nil) then
			current.draw()
		end
	end,
	
	update = function()
		local current = MENU[1] or {}
		
		if (current.update ~= nil) then
			current.update()
		end
	end,
	
	write = function(text,x_,y_,m_)
		local m = m_ or 1.0
		
		local x = x_ - #text * 0.25 * textsys.font.size() * m
		local y = y_ - 0.5 * textsys.font.size() * m
		love.graphics.print(text, x, y, nil, m, m)
	end,
	
	current = function()
		local current = MENU[1] or {}
		MENU.name = current.name or ""
		
		return MENU.name
	end,
	
	button =
	{
		create = function(name,x,y,id,data)
			local d = data or {}
			
			BUTTONS[#BUTTONS+1] = {}
			local b = BUTTONS[#BUTTONS]
			b.id = id or "button"
			b.name = name
			b.x = x
			b.y = y
			b.l = d.layer or 1
			b.w = d.w or 48
			b.h = d.h or 32
			b.text = d.text
			b.c = d.colour or 0xffffff
			b.disabled = data.disabled or false
			b.visible = true
			
			if (d._visible ~= nil) then
				b.visible = d._visible
			end
			
			if (d._sprite ~= nil) then
				local sprite = d._sprite
				local anim = d._anim
				local visible = true
				if (d._visible ~= nil) then
					visible = d._visible
				end
				
				b.sprite = spritesys.set(b,sprite,anim,visible)
			end
			
			if (d._text ~= nil) then
				b.text = d._text
				textsys.write(d._text,x,y,b.id,{font = d._font, layer = b.l, xoffset = -0.5, yoffset = -0.5})
			end
			
			return b
		end,
		
		draw = function(l)
			love.graphics.setLineWidth(scaling)
			local x,y = love.mouse.getPosition()
			
			for i,b in ipairs(BUTTONS) do
				if (b.l == l) and b.visible then
					local rw = b.w
					local rh = b.h
					local rx = b.x - rw * 0.5
					local ry = b.y - rh * 0.5
					if integral then
						rx = math.floor(rx)
						ry = math.floor(ry)
						rw = math.floor(rw)
						rh = math.floor(rh)
					end
					rx = rx * scaling
					ry = ry * scaling
					rw = rw * scaling
					rh = rh * scaling
					
					local style = "line"
					if (inputsys.ignore == false) and (((x >= rx) and (y >= ry) and (x < rx+rw) and (y < ry+rh)) or b.disabled) then
						style = "fill"
					end
					
					if (b.disabled == false) then
						love.graphics.setColor(getrgb(b.c))
					else
						love.graphics.setColor(0.6,0.6,0.6,0.5)
					end
					love.graphics.rectangle(style,rx,ry,rw,rh)
					
					if (b.sprite ~= nil) then
						spritesys.draw(b.sprite,b.x,b.y)
					end
				end
			end
		end,
		
		erase = function(id)
			local these = {}
			
			for i,b in ipairs(BUTTONS) do
				if (id == nil) or (b.id == id) then
					table.insert(these, i)
					
					if (b.text ~= nil) then
						textsys.erase(id)
					end
				end
			end
			
			for i,v in ipairs(these) do
				table.remove(BUTTONS, v-(i-1))
			end
		end,
		
		clickcheck = function(x,y,butt)
			for i,b in ipairs(BUTTONS) do
				local bx = b.x
				local by = b.y
				local w,h = b.w,b.h
				
				if (w > 0) and (h > 0) and (b.disabled == false) and (delay == 0) then
					if (x >= (bx - w * 0.5) * scaling) and (y >= (by - h * 0.5) * scaling) and (x < (bx + w * 0.5) * scaling) and (y < (by + h * 0.5) * scaling) then
						menusys.button.click(b,butt)
					end
				end
			end
		end,
		
		click = function(b,butt)
			if (b.func ~= nil) then
				b.func(b,butt)
			else
				local id = "button_" .. b.name
				inputsys.input(id,{butt,b})
			end
		end,
		
		enable = function(id,vis)
			for i,b in ipairs(BUTTONS) do
				if (id == nil) or (b.id == id) then
					b.disabled = false
					
					if (vis ~= nil) then
						b.visible = vis
					end
				end
			end
		end,
		
		disable = function(id,vis)
			for i,b in ipairs(BUTTONS) do
				if (id == nil) or (b.id == id) then
					b.disabled = true
					
					if (vis ~= nil) then
						b.visible = vis
					end
				end
			end
		end,
	},
}

BUTTONS = {}
MENU = {}
menulist = {}