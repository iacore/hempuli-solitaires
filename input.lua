--[[

function love.mousereleased(x, y, button, istouch)
   1 = left
   2 = right?
end

function love.keyreleased(key)
end
]]--

inputlist =
{
	main =
	{	
		escape = function(key)
			if (menusys.current() == "main") then
				menusys.sub("pause")
			end
		end,
		
		button_start = function(key,data)
			if (data[1] == 1) then
				delay = 5
				local icon = vars.selected
				menusys.sub("ingame")
				vars.selected = nil
				game = icon.name
				BACKCOL = icon.back
				startgame(icon.name)
				
				local x = screenw - 20
				local y = 20
				
				menusys.button.create("close",x,y,"ui",{_sprite = "button", _anim = "close", w = 18, h = 18})
				menusys.button.create("restart",x - 24,y,"ui",{_sprite = "button", _anim = "restart", w = 18, h = 18})
				
				if (scripts.info ~= nil) then
					menusys.button.create("info",x - 48,y,"ui",{_sprite = "button", _anim = "info", w = 18, h = 18})
				end
				
				if (scripts.autostack ~= nil) then
					menusys.button.create("stack",x - 72,y,"ui",{_sprite = "button", _anim = "stack", w = 18, h = 18})
				end
			end
		end,
		
		mouse_left = function(key,data)
			local u = data.target or inputsys.gethover("icon")
			local thumb = unitsys.find("thumbnail")
			
			if (#u > 0) then
				local icon = u[1]
				
				for i,v in ipairs(SCRIPT_MODS) do
					if (v == icon.name) then
						vars.selected = icon
						
						for a,b in ipairs(UNITS) do
							if (b.class == "icon") then
								b.y = b.oy
							end
						end
						
						local y = screenh * 0.5
						textsys.erase("info")
						menusys.button.erase("game")
						local t = textsys.write("$C" .. icon.titlecol .. icon.title,16,y,"info",{font = "header", wrap = screenw-32})
						y = y + t.h
						t = textsys.write(icon.desc,16,y,"info",{wrap = screenw-32})
						y = y + t.h + 4
						t = textsys.write("$C" .. icon.titlecol .. "DIFFICULTY: $Cffce4d" .. icon.rating,16,y,"info",{font = "header", wrap = screenw-32})
						y = y + t.h
						t = textsys.write("$C" .. icon.titlecol .. "Playable with a standard deck: $Ca7d962" .. icon.deck,16,y,"info",{wrap = screenw-32})
						y = y + t.h + 4
						t = textsys.write("$C" .. icon.titlecol .. "Wins: $Cffffff" .. tostring(icon.wins) .. "  " .. "$C" .. icon.titlecol .. "Highest streak: $Cffffff" .. tostring(icon.mstreak),16,y,"info",{wrap = screenw-32})
						
						menusys.button.create("start",screenw - 56,screenh - 48,"game",{_text = "Play", w = 96, h = 80})
						
						spritesys.anim.set(thumb.sprite,icon.name)
						
						icon.y = icon.oy - 24
						break
					end
				end
			else
				vars.selected = nil
				for a,b in ipairs(UNITS) do
					if (b.class == "icon") then
						b.y = b.oy
					end
				end
				textsys.erase("info")
				menusys.button.erase("game")
				spritesys.anim.set(thumb.sprite,"start")
			end
		end,
	},
	
	ingame =
	{
		r = function()
			if (vars.victory == 0) then
				local save = filesys.ini.open("save.txt")
				local streak = "0"
				save:update("streak",game,tostring(streak))
				save:close()
			end
			
			if (scripts.restart ~= nil) then
				scripts.restart()
			else
				startgame(game)
			end
		end,
		
		button_restart = function()
			inputsys.input("r")
		end,
		
		button_info = function()
			if (scripts.info ~= nil) then
				menusys.sub("info")
			end
		end,
		
		button_ready = function()
			if (scripts.ready ~= nil) then
				scripts.ready()
			end
		end,
		
		button_close = function()
			inputsys.input("escape")
		end,
		
		escape = function()
			BACKCOL = 0x000000
			delay = 5
			
			--[[
			if (vars.victory == 0) then
				local save = filesys.ini.open("save.txt")
				local streak = save:read("streak",game) or "0"
				save:update("streak",game,tostring(streak))
				save:close()
			end
			]]--
			
			if (scripts.cleanup ~= nil) then
				scripts.cleanup()
			else
				menusys.button.erase()
				textsys.erase()
				unitsys.reset()
				spritesys.init()
			end
			
			textsys.erase("wincount")
			menusys.button.erase("ui")
			scriptsys.setup()
			menusys.close()
			menusys.change("main")
			vars.selected = nil
			-- inputsys.input("mouse_left",{0,0,target = {unitsys.find(nil,game)}})
			game = ""
		end,
		
		z = function()
			undo()
		end,
		
		mouse_left = function(key,data)
			if (scripts.mouse_left ~= nil) then
				scripts.mouse_left(key,data)
			end
		end,
		
		mouse_right = function(key,data)
			if (scripts.mouse_right ~= nil) then
				scripts.mouse_right(key,data)
			end
		end,
		
		s = function()
			if (scripts.autostack ~= nil) then
				scripts.autostack()
			end
		end,
		
		button_stack = function()
			inputsys.input("s")
		end,
	},
	
	menu =
	{
		escape = function(key)
			if (MENU.name == "pause") then
				menusys.close()
			end
		end,
	},
	
	info =
	{
		button_closeinfo = function()
			menusys.button.erase("info")
			textsys.erase("info")
			unitsys.remove_byclass("info")
			menusys.button.enable("ui",true)
			ORDER[#ORDER] = nil
			menusys.close()
		end,
	},
}