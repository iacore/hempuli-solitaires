MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.change("card",poker_sprites.card)
		spritesys.add("suiticon","gfx/suiticons",poker_sprites.suiticon)
		spritesys.add("info1","gfx/poker_info1")
		
		local origin = 32
		local y = 96 + 16
		local slotdata_end = {open = false, open_place = true, _anim = "pile_white", check_place = scripts.pile_check_place_end}
		piles.end1 = unitsys.create("pile","end1",origin + 56 * 5,y,slotdata_end)
		piles.end2 = unitsys.create("pile","end2",origin + 56 * 6,y,slotdata_end)
		piles.end3 = unitsys.create("pile","end3",origin + 56 * 7,y,slotdata_end)
		piles.end4 = unitsys.create("pile","end4",origin + 56 * 8,y,slotdata_end)
		piles.end5 = unitsys.create("pile","end5",origin + 56 * 9,y,slotdata_end)
		piles.discard = unitsys.create("pile","discard",origin + 56 * 5,y + 84,{open = false, _anim = "pile_dark"})
		
		y = 56
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 56 * 4,y,{open = false, card_offsets = {0,-0.5}, _visible = false})
		piles.slot1 = unitsys.create("pile","slot1",origin + 56 * 0,y,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 56 * 1,y,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 56 * 2,y,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 56 * 3,y,slotdata)
		piles.slot5 = unitsys.create("pile","slot5",origin + 56 * 4,y,slotdata)
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","b13","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
		
		scripts.listresults()
	end,
	
	cleanup = function()
		textsys.erase("results")
		unitsys.reset()
		spritesys.change("card",SPRITELIST.card)
		spritesys.delete("suiticon")
		spritesys.delete("info1")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Cards are stacked in the $Ceb8edeleft slots$CW in a descending order.",
		"- A single card can be placed in each of the $Ceb8ede5 slots$CW on the right (after this, it can't be removed). Once all $Ceb8ede5 slots$CW are full, the cards in them are evaluated as a $Ce3c188poker hand$CW. Only the best possible $Ce3c188hand$CW is considered, and $Ca7d962points$CW are awarded as a $Ca7d962sum of the values of the cards$CW that contributed to the $Ce3c188hand$CW. If the given $Ce3c188hand$CW had already been awarded previously, the score will be updated if the new one scored higher.",
		"- The goal is to accumulate $Ca7d962240 points or more$CW in total. You do not have to get points for each different $Ce3c188hand$CW there is as long as the score is high enough.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
		textsys.write("Pair - 12 points",screenw * 0.5,screenh * 0.5 + 90,"info",{layer = l, font = "header", xoffset = -0.5})
		unitsys.create("info","info1",screenw * 0.5,screenh * 0.5 + 42,{_sprite = "info1", layer = l})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					local slot = "slot" .. tostring(piletarget+1)
					spritesys.anim.set(card.sprite,card.name)
					cardtopile(card,slot)
					
					piletarget = (piletarget + 1) % 4
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (delay == 0) then
			if (#piles.end1.cards > 0) and (#piles.end2.cards > 0) and (#piles.end3.cards > 0) and (#piles.end4.cards > 0) and (#piles.end5.cards > 0) and (#piles.hand.cards == 0) then
				scripts.evaluate()
				delay = 5
			else
				inputsys.ignore = false
				
				if (vars.total >= vars.goal) then
					victory()
				end
			end
		end
	end,
	
	evaluate = function()
		local hand = {piles.end1.cards[1], piles.end2.cards[1], piles.end3.cards[1], piles.end4.cards[1], piles.end5.cards[1]}
		local handname = "high"
		
		local suitmatch = true
		if (hand[1].suit ~= hand[2].suit) or (hand[1].suit ~= hand[3].suit) or (hand[1].suit ~= hand[4].suit) or (hand[1].suit ~= hand[5].suit) then
			suitmatch = false
		end
		
		local vals = {hand[1].value, hand[2].value, hand[3].value, hand[4].value, hand[5].value}
		local vals2 = {hand[1].value, hand[2].value, hand[3].value, hand[4].value, hand[5].value}
		for i,v in ipairs(vals) do
			if (v == 1) then
				vals[i] = 14
			end
		end
		
		local ordermatch = true
		local ordermatch2 = true
		vals = sortvalues(vals)
		vals2 = sortvalues(vals2)
		
		local v_ = vals[1] - 1
		for i,v in ipairs(vals) do
			if (v ~= v_+1) then
				ordermatch = false
				break
			end
			v_ = v
		end
		v_ = vals2[1] - 1
		for i,v in ipairs(vals2) do
			if (v ~= v_+1) then
				ordermatch2 = false
				break
			end
			v_ = v
		end
		
		local total = max(vals)
		
		if ordermatch2 and (ordermatch == false) then
			ordermatch = true
			vals = vals2
		end
		
		if suitmatch and ordermatch then
			handname = "sflush"
			total = sum(vals)
		elseif suitmatch then
			handname = "flush"
			total = sum(vals)
		elseif ordermatch then
			handname = "straight"
			total = sum(vals)
		end
		
		local m1,m2,m1v,m2v = scripts.findmatches(vals)
		
		if (m1 == 4) then
			handname = "four"
			total = m1v * m1
		elseif ((m1 == 3) and (m2 == 2)) or ((m1 == 2) and (m2 == 3)) then
			handname = "fhouse"
			total = m1v * m1 + m2v * m2
		elseif (m1 == 3) then
			handname = "three"
			total = m1v * 3
		elseif (m1 == 2) and (m2 == 2) then
			handname = "dtwo"
			total = m1v * 2 + m2v * 2
		elseif (m1 == 2) then
			handname = "two"
			total = m1v * 2
		end
		
		-- alert(hand .. ", " .. tostring(m1v) .. "/" .. tostring(m1) .. " " .. tostring(m2v) .. "/" .. tostring(m2))
		for i,c in ipairs(hand) do
			spritesys.anim.set(c.sprite,"back")
			cardtopile(c,"discard")
		end
		
		if (handname == "sflush") and (vals[#vals] == 14) then
			if (vars.results.rflush == nil) then
				vars.results.rflush = {total, hand[1].name, hand[2].name, hand[3].name, hand[4].name, hand[5].name}
			else
				if (total > vars.results.rflush[1]) then
					vars.results.rflush = {math.max(total, vars.results.rflush[1]), hand[1].name, hand[2].name, hand[3].name, hand[4].name, hand[5].name}
				end
			end
		elseif (vars.results[handname] == nil) then
			vars.results[handname] = {total, hand[1].name, hand[2].name, hand[3].name, hand[4].name, hand[5].name}
		else
			if (total > vars.results[handname][1]) then
				vars.results[handname] = {math.max(total, vars.results[handname][1]), hand[1].name, hand[2].name, hand[3].name, hand[4].name, hand[5].name}
			end
		end
		
		scripts.listresults()
	end,
	
	listresults = function()
		textsys.erase("results")
		unitsys.remove_byclass("suiticon")
		local data = {{"high","High card"},{"two","Pair"},{"dtwo","Two pairs"},{"three","Three of a kind"},{"straight","Straight"},{"flush","Flush"},{"fhouse","Full house"},{"four","Four of a kind"},{"sflush","Straight flush"},{"rflush","Royal flush"}}
		local x = screenw * 0.5 + 64
		local y = screenh * 0.5 - 56 + 32
		local total = 0
		
		for i,v in ipairs(data) do
			local text = v[2]
			local value = "0"
			
			if (vars.results[v[1]] == nil) then
				text = "$C979797" .. text
				value = "$C979797" .. value
			else
				text = " " .. text
				value = tostring(vars.results[v[1]][1])
				total = total + vars.results[v[1]][1]
				
				for j=1,5 do
					local n = vars.results[v[1]][j+1]
					unitsys.create("suiticon",n,x + 160 + j * 9,y+(i-1)*15 + 5,{_anim = n})
				end
			end
			
			textsys.write(text,x,y+(i-1)*15,"results",{font = "header", layer = 0})
			textsys.write(tostring(value),x + 160,y+(i-1)*15,"results",{font = "header", xoffset = -1.0, layer = 0})
		end
		
		textsys.write("Total",x,y+(#data)*15+2,"results",{font = "header", layer = 0})
		textsys.write(tostring(total),x + 160,y+(#data)*15+2,"results",{font = "header", xoffset = -1.0, layer = 0})
		textsys.write("$Cdfab53Goal",x,y+(#data+1)*15+2,"results",{font = "header", layer = 0})
		textsys.write("$Cdfab53" .. tostring(vars.goal),x + 160,y+(#data+1)*15+2,"results",{font = "header", xoffset = -1.0, layer = 0})
		
		vars.total = total
	end,
	
	findmatches = function(values)
		local done = {}
		local m1,m2 = 1,1
		local m1v,m2v = 0,0
		
		for i,v in ipairs(values) do
			if (done[i] == nil) then
				for a,b in ipairs(values) do
					if (done[a] == nil) and (a ~= i) then
						if (v == b) then
							if (m1v == 0) then
								m1v = v
							elseif (m2v == 0) and (v ~= m1v) then
								m2v = v
							end
							
							if (v == m1v) then
								m1 = m1 + 1
							elseif (v == m2v) then
								m2 = m2 + 1
							end
							
							done[a] = 1
						end
					end
				end
				
				done[i] = 1
			end
		end
		
		return m1,m2,m1v,m2v
	end,
	
	mouse_left = function(key,data)
		if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
						
			if pile.open or pile.open_pick then
				scripts.pick_card(unit)
				delay = 5
			end
		end
		
		if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
			scripts.place_card()
			delay = 5
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do					
					cardtopile(v,targetpile)
				end
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit+3
		local v = card.value+1
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			if (c.value ~= v-1) then
				return false
			end
			s = c.suit
			v = c.value
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local s = card.suit
		local v = card.value
		local op = card.oldpile
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			if (c.value ~= v+1) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		if (#piles.hand.cards > 1) then
			return false
		end
		
		if (#pile.cards > 0) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	results = {},
	goal = 240,
	total = 0,
}

poker_sprites =
{
	card =
	{
		file = "gfx/deck_thing",
		w = 48,
		h = 80,
		default = "back",
		anim =
		{
			back = {0,4},
			lock_1 = {1,4},
			lock_2 = {2,4},
			
			a1 = {0,0},
			a2 = {1,0},
			a3 = {2,0},
			a4 = {3,0},
			a5 = {4,0},
			a6 = {5,0},
			a7 = {6,0},
			a8 = {7,0},
			a9 = {8,0},
			a10 = {9,0},
			a11 = {10,0},
			a12 = {11,0},
			a13 = {12,0},
			
			b1 = {0,1},
			b2 = {1,1},
			b3 = {2,1},
			b4 = {3,1},
			b5 = {4,1},
			b6 = {5,1},
			b7 = {6,1},
			b8 = {7,1},
			b9 = {8,1},
			b10 = {9,1},
			b11 = {10,1},
			b12 = {11,1},
			b13 = {12,1},
			
			c1 = {0,2},
			c2 = {1,2},
			c3 = {2,2},
			c4 = {3,2},
			c5 = {4,2},
			c6 = {5,2},
			c7 = {6,2},
			c8 = {7,2},
			c9 = {8,2},
			c10 = {9,2},
			c11 = {10,2},
			c12 = {11,2},
			c13 = {12,2},
			
			d1 = {0,3},
			d2 = {1,3},
			d3 = {2,3},
			d4 = {3,3},
			d5 = {4,3},
			d6 = {5,3},
			d7 = {6,3},
			d8 = {7,3},
			d9 = {8,3},
			d10 = {9,3},
			d11 = {10,3},
			d12 = {11,3},
			d13 = {12,3},
		},
	},
	suiticon =
	{
		file = "gfx/suiticon",
		w = 7,
		h = 13,
		default = "a1",
		anim =
		{
			a1 = {0,0},
			a2 = {1,0},
			a3 = {2,0},
			a4 = {3,0},
			a5 = {4,0},
			a6 = {5,0},
			a7 = {6,0},
			a8 = {7,0},
			a9 = {8,0},
			a10 = {9,0},
			a11 = {10,0},
			a12 = {11,0},
			a13 = {12,0},
			
			b1 = {0,1},
			b2 = {1,1},
			b3 = {2,1},
			b4 = {3,1},
			b5 = {4,1},
			b6 = {5,1},
			b7 = {6,1},
			b8 = {7,1},
			b9 = {8,1},
			b10 = {9,1},
			b11 = {10,1},
			b12 = {11,1},
			b13 = {12,1},
			
			c1 = {0,2},
			c2 = {1,2},
			c3 = {2,2},
			c4 = {3,2},
			c5 = {4,2},
			c6 = {5,2},
			c7 = {6,2},
			c8 = {7,2},
			c9 = {8,2},
			c10 = {9,2},
			c11 = {10,2},
			c12 = {11,2},
			c13 = {12,2},
			
			d1 = {0,3},
			d2 = {1,3},
			d3 = {2,3},
			d4 = {3,3},
			d5 = {4,3},
			d6 = {5,3},
			d7 = {6,3},
			d8 = {7,3},
			d9 = {8,3},
			d10 = {9,3},
			d11 = {10,3},
			d12 = {11,3},
			d13 = {12,3},
		},
	},
}