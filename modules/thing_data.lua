MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.change("card",thing_sprites.card)
		spritesys.change("pile",{file = "gfx/pile_thing"})
		
		local origin = 48
		local y = 64
		local slotdata_end = {open = false, open_place = true, _anim = "pile_a", check_place = scripts.pile_check_place_end}
		piles.end1 = unitsys.create("pile","end1",origin + 64 * 5,y + 88 * 0,slotdata_end)
		slotdata_end._anim = "pile_b"
		piles.end2 = unitsys.create("pile","end2",origin + 64 * 5,y + 88 * 1,slotdata_end)
		slotdata_end._anim = "pile_c"
		piles.end3 = unitsys.create("pile","end3",origin + 64 * 5,y + 88 * 2,slotdata_end)
		slotdata_end._anim = "pile_d"
		piles.end4 = unitsys.create("pile","end4",origin + 64 * 5,y + 88 * 3,slotdata_end)
		
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place, _anim = "pile_white"}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 64 * 0,y,{open = false, card_offsets = {0,-0.5}, _visible = false})
		piles.slot1 = unitsys.create("pile","slot1",origin + 64 * 1,y,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 64 * 2,y,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 64 * 3,y,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 64 * 4,y,slotdata)
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","b13","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
	end,
	
	cleanup = function()
		unitsys.reset()
		spritesys.change("card",SPRITELIST.card)
		spritesys.change("pile",SPRITELIST.pile)
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Pile cards of every suit in $Ce3c188ascending order$CW on the slots on the right.",
		"- You can place cards on each other in an $Ce3c188alternating even/odd pattern$CW.",
		"- You can pick up a stack of any size as long as all the cards are of the $Cfbaff0same suit$CW.",
		"- You can also stack cards of the $Cfbaff0same value$CW, and move them together as a stack.",
		"- Press $Ce3c188S$CW to autostack.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					local slot = "slot" .. tostring(piletarget+1)
					spritesys.anim.set(card.sprite,card.name)
					cardtopile(card,slot)
					
					piletarget = (piletarget + 1) % 4
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (delay == 0) then
			if (vars.autostack == 1) then
				scripts.autostack()
			else
				inputsys.ignore = false
				
				if (#piles.end1.cards == 13) and (#piles.end2.cards == 13) and (#piles.end3.cards == 13) and (#piles.end4.cards == 13) then
					victory()
				end
			end
		end
	end,
	
	autostack = function()
		vars.autostack = 0
		inputsys.ignore = false
		
		for i,unit in ipairs(UNITS) do
			if (unit.class == "card") and (delay == 0) and (#piles.hand.cards == 0) and (vars.mode == 1) and (#unit.pile > 0) and (unit.pile ~= "hand") and (unit.pile ~= "deck") then
				local suitpilename = "end" .. tostring(unit.suit)
				local suitpile = piles[suitpilename]
				local pile = piles[unit.pile]
				
				if (unit.pileid == #pile.cards) and (unit.value == #suitpile.cards+1) then
					inputsys.ignore = true
					vars.autostack = 1
					delay = 5
					cardtopile(unit,suitpilename)
					return
				end
			end
		end
	end,
	
	mouse_left = function(key,data)
		if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
						
			if pile.open or pile.open_pick then
				scripts.pick_card(unit)
				delay = 5
			end
		end
		
		if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
			scripts.place_card()
			delay = 5
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do					
					cardtopile(v,targetpile)
				end
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit
		local v = card.value
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			if (c.suit ~= s) and (c.value ~= v) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		local op = card.oldpile
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			if (c.value % 2 == v % 2) and (c.value ~= v) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		
		local suitpilename = "end" .. tostring(card.suit)
		if (pile.name ~= suitpilename) then
			return false
		end
		
		if (#piles.hand.cards > 1) then
			return false
		end
		
		if (#pile.cards > 0) then
			local c = pile.cards[#pile.cards]
			
			if (c.value ~= v-1) or (c.suit ~= card.suit) then
				return false
			end
		elseif (card.value ~= 1) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	autostack = 0,
}

thing_sprites =
{
	card =
	{
		file = "gfx/deck_thing",
		w = 48,
		h = 80,
		default = "back",
		anim =
		{
			back = {0,4},
			lock_1 = {1,4},
			lock_2 = {2,4},
			
			a1 = {0,0},
			a2 = {1,0},
			a3 = {2,0},
			a4 = {3,0},
			a5 = {4,0},
			a6 = {5,0},
			a7 = {6,0},
			a8 = {7,0},
			a9 = {8,0},
			a10 = {9,0},
			a11 = {10,0},
			a12 = {11,0},
			a13 = {12,0},
			
			b1 = {0,1},
			b2 = {1,1},
			b3 = {2,1},
			b4 = {3,1},
			b5 = {4,1},
			b6 = {5,1},
			b7 = {6,1},
			b8 = {7,1},
			b9 = {8,1},
			b10 = {9,1},
			b11 = {10,1},
			b12 = {11,1},
			b13 = {12,1},
			
			c1 = {0,2},
			c2 = {1,2},
			c3 = {2,2},
			c4 = {3,2},
			c5 = {4,2},
			c6 = {5,2},
			c7 = {6,2},
			c8 = {7,2},
			c9 = {8,2},
			c10 = {9,2},
			c11 = {10,2},
			c12 = {11,2},
			c13 = {12,2},
			
			d1 = {0,3},
			d2 = {1,3},
			d3 = {2,3},
			d4 = {3,3},
			d5 = {4,3},
			d6 = {5,3},
			d7 = {6,3},
			d8 = {7,3},
			d9 = {8,3},
			d10 = {9,3},
			d11 = {10,3},
			d12 = {11,3},
			d13 = {12,3},
		},
	},
}