MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.change("card",{file="gfx/deck_eldritch"})
		spritesys.change("pile",{file="gfx/pile_eldritch"})
		spritesys.add("pile_multiple","gfx/pile_multiple",{xoffset = 16})
		spritesys.add("info1","gfx/eldritch_info1")
		
		local origin = 48
		local y = 64
		piles.atk1 = unitsys.create("pile","atk1",origin + 64 * 6,y + 88 * 0,{_sprite = "pile_multiple", card_offsets = {32,0}, check_place = scripts.attack_check_place, pick = scripts.attack_pick, atk = 0})
		piles.atk2 = unitsys.create("pile","atk2",origin + 64 * 6,y + 88 * 1,{_sprite = "pile_multiple", card_offsets = {32,0}, check_place = scripts.attack_check_place, pick = scripts.attack_pick, atk = 0})
		piles.atk3 = unitsys.create("pile","atk3",origin + 64 * 6,y + 88 * 2,{_sprite = "pile_multiple", card_offsets = {32,0}, check_place = scripts.attack_check_place, pick = scripts.attack_pick, atk = 0})
		piles.atk4 = unitsys.create("pile","atk4",origin + 64 * 6,y + 88 * 3,{_sprite = "pile_multiple", card_offsets = {32,0}, check_place = scripts.attack_check_place, pick = scripts.attack_pick, atk = 0})
		
		piles.mon1 = unitsys.create("pile","mon1",origin + 64 * 5,y + 88 * 0,{open = false})
		piles.mon2 = unitsys.create("pile","mon2",origin + 64 * 5,y + 88 * 1,{open = false})
		piles.mon3 = unitsys.create("pile","mon3",origin + 64 * 5,y + 88 * 2,{open = false})
		piles.mon4 = unitsys.create("pile","mon4",origin + 64 * 5,y + 88 * 3,{open = false})
		
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 64 * 0,y,{open = false, card_offsets = {0,-0.5}, _anim = "pile_dark"})
		piles.slot1 = unitsys.create("pile","slot1",origin + 64 * 1,y,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 64 * 2,y,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 64 * 3,y,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 64 * 4,y,slotdata)
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","b13","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
		
		local initmon = 0
		for i=1,4 do
			local id = i
			local c = piles.deck.cards[id]
			
			while (c.value > 9) and (id <= 52) do
				initmon = initmon + 1
				id = id + 4
				c = piles.deck.cards[id]
			end
		end
		if (initmon > 3) or (initmon == 0) then
			-- alert("Reshuffle")
			startgame("eldritch")
		end
	end,
	
	cleanup = function()
		unitsys.reset()
		spritesys.change("card",SPRITELIST.card)
		spritesys.change("pile",SPRITELIST.pile)
		spritesys.delete("pile_multiple")
		spritesys.delete("info1")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Defeat all $Ca7d96216$CW monsters by matching them with cards of $Ce3c188equal value$CW.",
		"- When a monster card is on the top of one of the $Ce971ca5 middle stacks$CW, it'll jump to an open $Ce971camonster slot$CW on the right. If there are no open $Ce971camonster slots$CW available, $Ce37269YOU LOSE$CW.",
		"- Every $Ce971camonster slot$CW has $C8bc8e72 attack slots$CW next to it. When the $Ce971camonster slot$CW is filled, you can place any number card in the $C8bc8e7attack slots$CW. If the $Ce3c188combined value$CW of the 2 cards in the $C8bc8e7attack slots$CW is $Ce3c188equal to the value$CW of the monster card, the monster is defeated and all 3 cards are discarded.",
		"- An $Ca7d962ace$CW is normally of value $Ca7d9621$CW, but if it's in an $C8bc8e7attack slot$CW for a monster of $Ce971camatching suit$CW, its value becomes $Ca7d9627$CW.",
		"- Number cards are stacked in either $Ce3c188ascending$CW or $Ce3c188descending$CW order by $Ce971caalternating suit$CW (i.e. a card can't be placed on a card of the $Ce971casame suit$CW).",
		"NOTE: In order to complete the solitaire, you need to use at least $Ca7d9621$CW ace with value $Ca7d9627$CW.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
		unitsys.create("info","info1",screenw * 0.5,screenh * 0.5 + 96,{_sprite = "info1", layer = l})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					local slot = "slot" .. tostring(piletarget+1)
					spritesys.anim.set(card.sprite,card.name)
					cardtopile(card,slot)
					
					piletarget = (piletarget + 1) % 4
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (delay == 0) then
			inputsys.ignore = false
			vars.attacks = 0
			
			if (delay == 0) and (#piles.hand.cards == 0) then
				for i=1,4 do
					local id = "slot" .. tostring(i)
					local atkid = "atk" .. tostring(i)
					local monid = "mon" .. tostring(i)
					local mon = piles[monid]
					
					local atk = piles[atkid]
					atk.atk = 0
					local these = {}
					if (#atk.cards > 0) then
						local monsuit = -1
						if (#mon.cards > 0) then
							local c = mon.cards[1]
							monsuit = c.suit
						end
						
						local atksuit = -1
						for j,c in ipairs(atk.cards) do
							local val = c.value
							if (atksuit == -1) then
								atksuit = c.suit
							end
							
							if (val == 1) and (monsuit == c.suit) then
								val = 7
							end
							
							-- if (atksuit == c.suit) then
								atk.atk = atk.atk + val
								table.insert(these, c)
							-- end
						end
					end
					if (atk.atk > 0) then
						vars.attacks = 1
					end
					
					if (#mon.cards > 0) and (delay == 0) then
						local c = mon.cards[1]
						table.insert(these, c)
						
						if (atk.atk == c.value) then
							delay = 10
							vars.defeated = vars.defeated + 1
							for j,v in ipairs(these) do
								spritesys.anim.set(v.sprite,"back")
								cardtopile(v,"deck")
							end
						end
					end
				end
				
				if (delay == 0) then
					for i=1,4 do
						local id = "slot" .. tostring(i)
						local pile = piles[id]
						if (#pile.cards > 0) and (delay == 0) then
							local card = pile.cards[#pile.cards]
							if (card.value > 9) then
								for j=1,4 do
									local id2 = "mon" .. tostring(j)
									local pile2 = piles[id2]
									
									if (#pile2.cards == 0) then
										cardtopile(card,id2)
										delay = 5
										inputsys.ignore = true
										return
									end
								end
								
								cardtopile(card,"deck")
								delay = 5
								vars.lost = true
								vars.mode = 2
								addloss(game)
								
								ORDER[#ORDER + 1] = {}
								local o = ORDER[#ORDER]
								local c = getrgb(0x523432)
								o.effect = function() love.graphics.setColor(c) love.graphics.rectangle("fill", 0, math.floor((screenh * 0.5 - 48)) * scaling, screenw * scaling, 96 * scaling) love.graphics.setColor(1, 1, 1, 1) end
								textsys.write("YOU DIED",screenw * 0.5,screenh * 0.5,"wincount",{font = "serif", xoffset = -0.5, yoffset = -0.5, layer = #ORDER})
								return
							end
						end
					end
				end
			end
			
			if (vars.defeated == 16) then
				victory()
			end
		end
	end,
	
	mouse_left = function(key,data)
		if (vars.lost == false) then
			if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
				local unit = targetcard
				local pile = unitsys.find("pile",unit.pile)
							
				if pile.open or pile.open_pick then
					if (pile.pick ~= nil) then
						pile.pick(unit)
					else
						scripts.pick_card(unit)
					end
					delay = 5
				end
			end
			
			if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
				if (targetpile.place ~= nil) then
					targetpile.place()
				else
					scripts.place_card()
				end
				delay = 5
			end
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	attack_pick = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		cardtopile(unit,"hand")
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do					
					cardtopile(v,targetpile)
				end
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit + 2
		local v = card.value + 1
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			--if (c.value > 9) or (math.floor(c.suit / 2) % 2 == math.floor(s / 2) % 2) or ((c.value ~= v + 1) and (c.value ~= v - 1)) then
			if (c.value > 9) or (c.suit == s) or ((c.value ~= v + 1) and (c.value ~= v - 1)) then
			-- if (c.value > 9) or ((c.value ~= v + 1) and (c.value ~= v - 1)) then
				return false
			end
			
			s = c.suit
			v = c.value
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		local op = card.oldpile
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			-- if (c.value > 9) or (math.floor(c.suit / 2) % 2 == math.floor(card.suit / 2) % 2) or ((c.value ~= v - 1) and (c.value ~= v + 1)) then
			if (c.value > 9) or (c.suit == card.suit) or ((c.value ~= v - 1) and (c.value ~= v + 1)) then
			-- if (c.value > 9) or ((c.value ~= v - 1) and (c.value ~= v + 1)) then
				return false
			end
		end
		
		return true
	end,
	
	attack_check_place = function(pile)
		local card = piles.hand.cards[1]
		local pilenum = string.sub(pile.name, -1)
		local mon = "mon" .. pilenum
		local monpile = piles[mon]
		
		if (#pile.cards >= 2) or (#piles.hand.cards > 1) or (card.value > 9) or (#monpile.cards == 0) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	attacks = 0,
	lost = false,
	defeated = 0,
}