MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.change("card",{file="gfx/deck_baba"})
		spritesys.change("pile",{file="gfx/pile_baba"})
		spritesys.add("info1","gfx/baba_info1")
		
		local origin = 48
		local y = 64
		local slotdata_end = {open = false, open_place = true, _anim = "pile_a", check_place = scripts.pile_check_place_end}
		piles.end1 = unitsys.create("pile","end1",424,y + 88 * 0,slotdata_end)
		slotdata_end._anim = "pile_b"
		piles.end2 = unitsys.create("pile","end2",424,y + 88 * 1,slotdata_end)
		slotdata_end._anim = "pile_c"
		piles.end3 = unitsys.create("pile","end3",424,y + 88 * 2,slotdata_end)
		slotdata_end._anim = "pile_d"
		piles.end4 = unitsys.create("pile","end4",424,y + 88 * 3,slotdata_end)
		
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 64 * 0,y,{open = false, card_offsets = {0,-0.5}})
		piles.slot6 = unitsys.create("pile","slot6",origin + 64 * 0,y,slotdata)
		piles.slot1 = unitsys.create("pile","slot1",origin + 64 * 1,y,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 64 * 2,y,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 64 * 3,y,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 64 * 4,y,slotdata)
		piles.slot5 = unitsys.create("pile","slot5",origin + 64 * 5,y,slotdata)
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","b13","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s, sideways = false})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
	end,
	
	cleanup = function()
		unitsys.reset()
		spritesys.change("card",SPRITELIST.card)
		spritesys.change("pile",SPRITELIST.pile)
		spritesys.delete("info1")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Pile cards of every suit in $Ce3c188ascending order$CW on the slots on the right.",
		"- Cards are stacked in the $Ceb8edemiddle slots$CW in $Ce3c188descending order$CW.",
		"- You can only pick up a stack of multiple cards if they're of the $Ceb8edesame suit$CW.",
		"- When you're holding a single card, you can right-click on another card to place the held card $Ca7d962sideways$CW on top of the clicked card. Any card can be placed on top of a $Ca7d962sideways card$CW, but you can only have $Ca7d9623 sideways cards$CW at maximum at a time.$S$S$S$S$S$S$S$S$S$S",
		"- To turn a $Ca7d962sideways card$CW back to normal, you need to pick it up and place it in a valid spot for that specific card.",
		"- Press $Ca7d962S$CW to autostack.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
		unitsys.create("info","info1",screenw * 0.5,screenh * 0.5,{_sprite = "info1", layer = l})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					local slot = "slot" .. tostring(piletarget+1)
					spritesys.anim.set(card.sprite,card.name)
					cardtopile(card,slot)
					
					piletarget = (piletarget + 1) % 4
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (delay == 0) then
			if (vars.autostack == 1) then
				scripts.autostack()
			else
				inputsys.ignore = false
				
				if (#piles.end1.cards == 13) and (#piles.end1.cards == 13) and (#piles.end1.cards == 13) and (#piles.end1.cards == 13) then
					victory()
				end
			end
		end
	end,
	
	card_pos = function(unit)
		if unit.sideways then
			unit.sprite.angle = unit.sprite.angle + (1.57075 - unit.sprite.angle) * 0.4
		else
			unit.sprite.angle = unit.sprite.angle + (0 - unit.sprite.angle) * 0.4
		end
	end,
	
	autostack = function()
		vars.autostack = 0
		inputsys.ignore = false
		
		for i,unit in ipairs(UNITS) do
			if (unit.class == "card") and (delay == 0) and (#piles.hand.cards == 0) and (vars.mode == 1) and (#unit.pile > 0) and (unit.pile ~= "hand") and (unit.pile ~= "deck") then
				local suitpilename = "end" .. tostring(unit.suit)
				local suitpile = piles[suitpilename]
				local pile = piles[unit.pile]
				
				if (unit.pileid == #pile.cards) and (unit.value == #suitpile.cards+1) then
					if unit.sideways then
						unit.sideways = false
						vars.sideways = vars.sideways - 1
					end
					
					inputsys.ignore = true
					vars.autostack = 1
					delay = 5
					cardtopile(unit,suitpilename)
					return
				end
			end
		end
	end,
	
	mouse_left = function(key,data)
		if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
						
			if pile.open or pile.open_pick then
				scripts.pick_card(unit)
				delay = 5
			end
		end
		
		if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
			scripts.place_card()
			delay = 5
		end
	end,
	
	mouse_right = function(key,data)
		if (targetcard ~= nil) and (#piles.hand.cards == 1) and (delay == 0) and (vars.sideways < 3) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
						
			if (pile.open or pile.open_place) and (#pile.cards > 0) and (piles.hand.cards[1].sideways == false) and (string.sub(pile.name, 1, 3) ~= "end") then
				scripts.place_sideways(piles.hand.cards[1],pile)
				delay = 5
			end
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_sideways = function(card,pile)
		card.sideways = true
		cardtopile(card,pile.name)
		vars.sideways = vars.sideways + 1
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do
					if v.sideways then
						local doit = false
						
						if (#pile.cards == 0) then
							doit = true
						else
							local card = pile.cards[#pile.cards]
							if (card.value == v.value + 1) then
								doit = true
							end
						end
						
						if (targetpile ~= v.oldpile) or doit then
							v.sideways = false
							vars.sideways = vars.sideways - 1
						end
					end
					
					cardtopile(v,targetpile)
				end
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit
		local v = card.value
		
		if card.sideways and (card.pileid < #pile.cards) then
			return false
		end
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			if (c.suit ~= s) or (c.value ~= v) or (c.sideways and (i > card.pileid)) then
				return false
			end
			
			v = v - 1
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		local op = card.oldpile
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			if ((c.value ~= v+1) and (c.sideways == false)) or (card.sideways and c.sideways) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		
		local suitpilename = "end" .. tostring(card.suit)
		if (pile.name ~= suitpilename) then
			return false
		end
		
		if (#piles.hand.cards > 1) then
			return false
		end
		
		if (#pile.cards > 0) then
			local c = pile.cards[#pile.cards]
			
			if (c.value ~= v-1) or (c.suit ~= card.suit) then
				return false
			end
		elseif (card.value ~= 1) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	sideways = 0,
	autostack = 0,
}