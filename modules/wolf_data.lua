MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		vars.selected = nil
		
		spritesys.change("card",wolf_sprites.card)
		spritesys.change("pile",{file = "gfx/pile_thing"})
		spritesys.add("info1","gfx/wolf_info1")
		spritesys.add("frame","gfx/wolf_frame")
		
		local x = 48
		local y = screenh * 0.6
		local slotdata_end = {open = false, open_place = true, _anim = "pile_a", check_place = scripts.pile_check_place_end}
		piles.end1 = unitsys.create("pile","end1",screenw - 72,76 + 84 * 0,slotdata_end)
		slotdata_end._anim = "pile_b"
		piles.end2 = unitsys.create("pile","end2",screenw - 72,76 + 84 * 1,slotdata_end)
		slotdata_end._anim = "pile_c"
		piles.end3 = unitsys.create("pile","end3",screenw - 72,76 + 84 * 2,slotdata_end)
		slotdata_end._anim = "pile_d"
		piles.end4 = unitsys.create("pile","end4",screenw - 72,76 + 84 * 3,slotdata_end)
		
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",x,y,{open = false, card_offsets = {0,-0.5}, _anim = "pile_dark", _visible = false})
		
		for i=1,8 do
			local name = "slot" .. tostring(i)
			local sdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place}
			piles[name] = unitsys.create("pile",name,x + 56 * (i-1),y,sdata)
			
			local name2 = "house" .. tostring(i)
			local sdata2 = {card_offsets = {0,-14}, open = false, visible = false}
			piles[name2] = unitsys.create("pile",name2,x + 56 * (i-1),y - 88,sdata2)
			-- piles[name].sprite.angle = a
		end
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","b13","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		local hides = {"a","b","c","d","e","f","g","h","i","j","k","l"}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			local h
			
			if (v > 10) then
				h = hides[1]
				table.remove(hides, 1)
			end
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s, hide = h, guess = ""})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
		
		menusys.button.create("ready",48,24,"wolf",{_text = "Ready", w = 80, h = 32, disabled = true})
	end,
	
	cleanup = function()
		unitsys.reset()
		menusys.button.erase("wolf")
		spritesys.change("card",SPRITELIST.card)
		spritesys.change("pile",SPRITELIST.pile)
		spritesys.delete("info1")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Pile cards of every suit in $Ce3c188ascending order$CW on the $Ceb8edeslots on the right$CW.",
		"- Cards are stacked in the $Ceb8edebottom slots$CW in an $Ca7d962alternating suit$CW pattern, either $Ce3c188ascending$CW or $Ce3c188descending$CW.",
		"- The $Ceb8edeface cards$CW are unknown and move from left to right in the $Ceb8edetop slots$CW based on their $Ca7d962suit/value$CW, determined by the cards in the $Ceb8edebottom slot$CW corresponding to each $Ceb8edetop slot$CW:$S$Ce3c188Jacks$CW: move above the next $Ceb8edebottom slot$CW with 0 cards of the $Ce3c188Jack$CW's $Ca7d962suit$CW, if one exists.$S$Ceb8edeQueens$CW: move above the next $Ceb8edebottom slot$CW tied for least cards of the $Ceb8edeQueen$CW's $Ca7d962suit$CW (not 0).$S$C8bc8e7Kings$CW: move above the next $Ceb8edebottom slot$CW with at least 1 card of the $C8bc8e7King$CW's $Ca7d962suit$CW, if one exists. $S$S$S$S$S$S$S$S$S",
		"- You need to use the above logic to determine the identity of each $Ceb8edeface card$CW. When you have a guess, click on a $Ceb8edeface card$CW and choose the card you believe it to be from the menu that opens.",
		"- When you've filled the $Ceb8edeslots on the right$CW and have given a guess for each $Ceb8edeface card$CW, the game ends. If your guesses were correct, $Ca7d962you win$CW. If not, $Ce37269YOU LOSE$CW.",
		"- Press $Ca7d962S$CW to autostack.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
		unitsys.create("info","info1",screenw * 0.5,screenh * 0.5 + 16,{_sprite = "info1", layer = l})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					spritesys.anim.set(card.sprite,card.name)
					
					if (card.value <= 10) then
						local slot = "slot" .. tostring(vars.pileid + 1)
						cardtopile(card,slot)
						vars.pileid = (vars.pileid + 1) % 8
					else
						local slot = "house" .. tostring(vars.houseid + 1)
						cardtopile(card,slot)
						vars.houseid = (vars.houseid + 1) % 8
						
						if (card.value > 10) then
							local h = card.hide
							spritesys.anim.set(card.sprite,h)
						end
					end
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 2
				vars.mode_timer = 0
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (vars.mode == 2) then
			for i,unit in ipairs(UNITS) do
				if (unit.class == "card") and (unit.value > 10) then
					local pname = scripts.nextpile(unit)
					
					if (pname ~= unit.pile) then
						local pile = piles[pname]
						local pos = 1
						if (#pile.cards < 0) then
							for a,b in ipairs(pile.cards) do
								if (b.value > unit.value) then
									pos = pos + 1
								end
							end
						end
						cardtopile(unit,pname,pos)
					else
						-- alert(unit.name .. " didn't move")
					end
				end
			end
			
			vars.mode = 1
		elseif (delay == 0) then
			if (vars.autostack == 1) then
				scripts.autostack()
			else
				inputsys.ignore = false
				
				if (#piles.end1.cards == 10) and (#piles.end2.cards == 10) and (#piles.end3.cards == 10) and (#piles.end4.cards == 10) then
					local guess = 0
					
					for i,unit in ipairs(UNITS) do
						if (unit.class == "card") and (unit.value > 9) and (#unit.guess > 0) then
							guess = guess + 1
						end
					end
					
					if (guess == 12) and (vars.ready == 0) then
						vars.ready = 1
						menusys.button.enable("wolf")
					elseif (guess < 12) and (vars.ready == 1) then
						vars.ready = 0
						menusys.button.disable("wolf")
					end
				end
			end
		end
	end,
	
	ready = function()
		local success = true
		menusys.button.disable("wolf")
		local errors = ""
		local suits = {"C", "S", "H", "D"}
		
		for i,unit in ipairs(UNITS) do
			if (unit.class == "card") and (unit.value > 10) then
				-- alert(unit.guess .. ", " .. unit.name)
				if (unit.guess ~= unit.name) then
					success = false
				else
					local s = suits[unit.suit]
					errors = errors .. s .. tostring(unit.value) .. ", "
				end
			end
		end
		
		if (#errors > 0) then
			errors = "Correct answers: " .. string.sub(errors, 1, #errors - 2)
		end
		
		if success then
			victory()
		else
			vars.lost = true
			addloss(game)
			
			ORDER[#ORDER + 1] = {}
			local o = ORDER[#ORDER]
			local c = getrgb(0x523432)
			o.effect = function() love.graphics.setColor(c) love.graphics.rectangle("fill", 0, math.floor((screenh * 0.5 - 48)) * scaling, screenw * scaling, 96 * scaling) love.graphics.setColor(1, 1, 1, 1) end
			textsys.write("YOU LOST",screenw * 0.5,screenh * 0.5 - 12,"wincount",{font = "serif", xoffset = -0.5, yoffset = -0.5, layer = #ORDER})
			textsys.write(errors,screenw * 0.5,screenh * 0.5 + 12,"wincount",{font = "header", xoffset = -0.5, yoffset = -0.5, layer = #ORDER})
		end
	end,
	
	autostack = function()
		if (vars.lost == false) then
			vars.autostack = 0
			inputsys.ignore = false
			
			for i,unit in ipairs(UNITS) do
				if (unit.class == "card") and (delay == 0) and (#piles.hand.cards == 0) and (unit.value < 11) and (vars.mode == 1) and (#unit.pile > 0) and (unit.pile ~= "hand") and (unit.pile ~= "deck") then
					local suitpilename = "end" .. tostring(unit.suit)
					local suitpile = piles[suitpilename]
					local pile = piles[unit.pile]
					
					if (unit.pileid == #pile.cards) and (unit.value == #suitpile.cards+1) then
						inputsys.ignore = true
						vars.autostack = 1
						vars.mode = 2
						delay = 5
						cardtopile(unit,suitpilename)
						return
					end
				end
			end
		end
	end,
	
	victory = function()
	end,
	
	nextpile = function(unit)
		local pname = unit.pile
		local id = tonumber(string.sub(pname, -1)) or 1
		local s = unit.suit
		local v = unit.value
		local check = "slot" .. tostring(id)
		
		if (v ~= 12) then
			for i=1,8 do
				id = (id % 8) + 1
				check = "slot" .. tostring(id)
				pname = "house" .. tostring(id)
				local pile = piles[check]
				
				if (#pile.cards == 0) and (v ~= 13) then
					return pname
				else
					local sfound = false
					for a,b in ipairs(pile.cards) do
						if (b.suit == s) then
							sfound = true
							break
						end
					end
					
					if (v == 11) and (sfound == false) then
						return pname
					end
					
					if (v == 13) and sfound then
						return pname
					end
				end
			end
		else
			local most = 999
			local mostname = pname
			
			for i=1,8 do
				id = (id % 8) + 1
				check = "slot" .. tostring(id)
				pname = "house" .. tostring(id)
				local pile = piles[check]
				
				if (#pile.cards > 0) then
					local count = 0
					
					for a,b in ipairs(pile.cards) do
						if (b.suit == s) then
							count = count + 1
						end
					end
					
					if (count > 0) and (count < most) then
						most = count
						mostname = pname
					end
				end
			end
			
			return mostname
		end
		
		return unit.pile
	end,
	
	mouse_left = function(key,data)
		if (vars.lost == false) then
			if (vars.mode == 1) then
				if (targetcard ~= nil) and (targetcard.value > 10) and (delay == 0) then
					vars.selected = targetcard
					local opts = {"a11","a12","a13","b11","b12","b13","c11","c12","c13","d11","d12","d13"}
					for i,opt in ipairs(opts) do
						local x = screenw * 0.5 - 2 * 56 + 56 * math.floor((i-1) * 0.5)
						local y = screenh * 0.5 - 44 + ((i-1) % 2) * 88
						local sprites
						
						for a,b in ipairs(UNITS) do
							if (b.class == "card") and (b.guess == opt) then
								sprites = {"frame"}
							end
						end
						
						unitsys.create("cardbutton","guess_" .. opt,x,y,{_sprite = "card", _anim = opt, layer = 2, _sprites = sprites})
					end
					
					unitsys.create("cardbutton","noguess",screenw * 0.5 - 3 * 56,screenh * 0.5 - 44,{_sprite = "card", _anim = "noguess", layer = 2})
					unitsys.create("cardbutton","cancel",screenw * 0.5 - 3 * 56,screenh * 0.5 + 44,{_sprite = "card", _anim = "cancel", layer = 2})
					
					local overlay = function() love.graphics.setColor(0, 0.25, 0.5, 0.95) love.graphics.rectangle("fill", 0, 0, screenw * scaling, screenh * scaling) love.graphics.setColor(1, 1, 1, 1) end
					-- ORDER[2] = {}
					ORDER[2].effect = overlay
					vars.mode = 3
					delay = 5
				end
				
				if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
					local unit = targetcard
					local pile = unitsys.find("pile",unit.pile)
								
					if pile.open or pile.open_pick then
						if (pile.pick ~= nil) then
							pile.pick(unit)
						else
							scripts.pick_card(unit)
						end
						delay = 5
					end
				end
				
				if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
					if (targetpile.place ~= nil) then
						targetpile.place()
					else
						scripts.place_card()
					end
					delay = 5
				end
			elseif (vars.mode == 3) and (delay == 0) then
				local u = inputsys.gethover("cardbutton",2)
				
				if (u[1] ~= nil) then
					local opt = u[1].name
					
					if (opt ~= "noguess") and (opt ~= "cancel") then
						vars.selected.guess = string.sub(opt, 7)
					elseif (opt == "noguess") then
						vars.selected.guess = ""
						opt = vars.selected.hide
					end
					
					if (opt ~= "cancel") then
						spritesys.anim.set(vars.selected.sprite,opt)
					end
					
					vars.mode = 1
					vars.selected = nil
					delay = 5
					ORDER[2].effect = nil
					unitsys.remove_byclass("cardbutton")
				end
			end
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				local c = piles.hand.cards[1]
				local op = c.oldpile
				
				if (pile.name ~= op) then
					vars.mode = 2
				end
				
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do					
					cardtopile(v,targetpile)
				end
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit + 2
		local v = card.value + 1
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			--if (c.value > 9) or (math.floor(c.suit / 2) % 2 == math.floor(s / 2) % 2) or ((c.value ~= v + 1) and (c.value ~= v - 1)) then
			if (c.value > 10) or (c.suit == s) or ((c.value ~= v + 1) and (c.value ~= v - 1)) then
			-- if (c.value > 9) or ((c.value ~= v + 1) and (c.value ~= v - 1)) then
				return false
			end
			
			s = c.suit
			v = c.value
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		local op = card.oldpile
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			-- if (c.value > 9) or (math.floor(c.suit / 2) % 2 == math.floor(card.suit / 2) % 2) or ((c.value ~= v - 1) and (c.value ~= v + 1)) then
			if (c.value > 10) or (c.suit == card.suit) or ((c.value ~= v - 1) and (c.value ~= v + 1)) then
			-- if (c.value > 9) or ((c.value ~= v - 1) and (c.value ~= v + 1)) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		
		local suitpilename = "end" .. tostring(card.suit)
		if (pile.name ~= suitpilename) then
			return false
		end
		
		if (#piles.hand.cards > 1) then
			return false
		end
		
		if (#pile.cards > 0) then
			local c = pile.cards[#pile.cards]
			
			if (c.value ~= v-1) or (c.suit ~= card.suit) then
				return false
			end
		elseif (card.value ~= 1) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	autostack = 0,
	houseid = 0,
	pileid = 0,
	ready = 0,
	lost = false,
}

wolf_sprites =
{
	card =
	{
		file = "gfx/deck_wolf",
		w = 48,
		h = 80,
		default = "back",
		anim =
		{
			back = {0,4},
			
			a1 = {0,0},
			a2 = {1,0},
			a3 = {2,0},
			a4 = {3,0},
			a5 = {4,0},
			a6 = {5,0},
			a7 = {6,0},
			a8 = {7,0},
			a9 = {8,0},
			a10 = {9,0},
			a11 = {10,0},
			a12 = {11,0},
			a13 = {12,0},
			
			b1 = {0,1},
			b2 = {1,1},
			b3 = {2,1},
			b4 = {3,1},
			b5 = {4,1},
			b6 = {5,1},
			b7 = {6,1},
			b8 = {7,1},
			b9 = {8,1},
			b10 = {9,1},
			b11 = {10,1},
			b12 = {11,1},
			b13 = {12,1},
			
			c1 = {0,2},
			c2 = {1,2},
			c3 = {2,2},
			c4 = {3,2},
			c5 = {4,2},
			c6 = {5,2},
			c7 = {6,2},
			c8 = {7,2},
			c9 = {8,2},
			c10 = {9,2},
			c11 = {10,2},
			c12 = {11,2},
			c13 = {12,2},
			
			d1 = {0,3},
			d2 = {1,3},
			d3 = {2,3},
			d4 = {3,3},
			d5 = {4,3},
			d6 = {5,3},
			d7 = {6,3},
			d8 = {7,3},
			d9 = {8,3},
			d10 = {9,3},
			d11 = {10,3},
			d12 = {11,3},
			d13 = {12,3},
			
			a = {1,4},
			b = {2,4},
			c = {3,4},
			d = {4,4},
			e = {5,4},
			f = {6,4},
			g = {7,4},
			h = {8,4},
			i = {9,4},
			j = {10,4},
			k = {11,4},
			l = {12,4},
			
			guess_a11 = {0,5},
			guess_a12 = {1,5},
			guess_a13 = {2,5},
			guess_b11 = {3,5},
			guess_b12 = {4,5},
			guess_b13 = {5,5},
			guess_c11 = {6,5},
			guess_c12 = {7,5},
			guess_c13 = {8,5},
			guess_d11 = {9,5},
			guess_d12 = {10,5},
			guess_d13 = {11,5},
			
			noguess = {12,5},
			cancel = {0,6},
		},
	},
}