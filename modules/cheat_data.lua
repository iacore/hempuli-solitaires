MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.change("card",cheat_sprites.card)
		spritesys.add("number","gfx/cheat_numbers",cheat_sprites.number)
		spritesys.add("info1","gfx/cheat_info1")
		vars.deal = 4
		
		local origin = 48
		local y = 64
		local slotdata_end = {open = false, open_place = true, _anim = "pile_white", check_place = scripts.pile_check_place_end, need = 5}
		piles.end1 = unitsys.create("pile","end1",origin + 64 * 5,y + 88 * 0,slotdata_end)
		piles.end2 = unitsys.create("pile","end2",origin + 64 * 5,y + 88 * 1,slotdata_end)
		piles.end3 = unitsys.create("pile","end3",origin + 64 * 5,y + 88 * 2,slotdata_end)
		
		piles.end1.number = unitsys.create("number","end1",piles.end1.x + 48,piles.end1.y)
		piles.end2.number = unitsys.create("number","end2",piles.end2.x + 48,piles.end2.y)
		piles.end3.number = unitsys.create("number","end3",piles.end3.x + 48,piles.end3.y)
		
		local slotdata = {card_offsets = {0,12}, check_place = scripts.pile_check_place}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,6}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 64 * 0,y + 88,{open = false, card_offsets = {0,-0.5}})
		piles.slot1 = unitsys.create("pile","slot1",origin + 64 * 1,y + 88,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 64 * 2,y + 88,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 64 * 3,y + 88,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 64 * 4,y + 88,slotdata)
		
		slotdata = {open = false}
		piles.pick1 = unitsys.create("pile","pick1",origin + 64 * 1,y,slotdata)
		piles.pick2 = unitsys.create("pile","pick2",origin + 64 * 2,y,slotdata)
		piles.pick3 = unitsys.create("pile","pick3",origin + 64 * 3,y,slotdata)
		piles.pick4 = unitsys.create("pile","pick4",origin + 64 * 4,y,slotdata)
		
		local cards = 104
		local left = cards * 0.5
		local suit = 0
		
		for i=1,cards do
			suit = 0
			if (left > 0) then
				suit = math.random(0,1)
				
				if (suit == 1) then
					left = left - 1
				end
			end
			
			local card = unitsys.create("card","c",piles.deck.x,piles.deck.y,{suit = suit})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
		end
	end,
	
	cleanup = function()
		unitsys.reset()
		spritesys.change("card",SPRITELIST.card)
		spritesys.delete("number")
		spritesys.delete("info1")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- The deck consists of $Ca7d962104$CW cards: $Ca7d96252$CW face-up and $Ca7d96252$CW face-down. Think of these as $Cfbaff02 suits$CW.",
		"- Cards of the $Cfbaff0same suit$CW join into a single $Cfbaff0group$CW when stacked. You can't move a card without moving its entire $Cfbaff0group$CW.$S$S$S$S$S$S$S$S$S",
		"- $Cfbaff0Groups$CW can be picked up from anywhere (even under other $Cfbaff0groups$CW), but only placed on $Ce3c188empty slots$CW or $Cfbaff0groups$CW of size $Ca7d9621$CW higher or lower. You can place a $Cfbaff0group$CW on another $Cfbaff0group$CW regardless of their $Cfbaff0suits$CW.",
		"- The goal is to place $Cfbaff0groups$CW of cards on the $Ce3c1883 slots$CW on the right so that the size of a $Cfbaff0group$CW is higher or equal to the number next to the $Ce3c188slot$CW.$SEach of the $Ce3c1883 slots$CW requires first $Ca7d9625$CW cards, then $Ca7d9627$CW, then $Ca7d9629$CW and then $Ca7d96211$CW. The $Cfbaff0groups$CW can be of either $Cfbaff0suit$CW.",
		"- Clicking on the deck deals a new card on $Ce3c188all 4 middle slots$CW. You can see the upcoming cards above the $Ce3c188slots$CW.",
		"NOTE: fulfilling the goal requires $Ca7d96296$CW cards at minimum, so you have a surplus of $Ca7d9628$CW cards at the start.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
		unitsys.create("info","info1",screenw * 0.5,screenh * 0.5 - 60,{_sprite = "info1", layer = l})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (piletarget < 4) then
					local card = pile.cards[#pile.cards]
					local slotid = "slot" .. tostring(piletarget+1)
					local slot = piles[slotid]
					if (card.suit == 0) then
						spritesys.anim.set(card.sprite,"open")
					else
						spritesys.anim.set(card.sprite,"open2")
					end
					cardtopile(card,slotid)
					
					if (#slot.cards >= piletarget+1) then
						piletarget = piletarget + 1
					end
				end
			end
			
			if (piletarget == 4) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (vars.mode == 2) then
			if (delay == 0) then
				local id = (vars.mode_timer % 4)+1
				local pickid = "pick" .. tostring(id)
				local slotid = "slot" .. tostring(id)
				
				if (#piles[pickid].cards > 0) then
					local card = piles[pickid].cards[#piles[pickid].cards]
					if (card.suit == 0) then
						spritesys.anim.set(card.sprite,"open")
					else
						spritesys.anim.set(card.sprite,"open2")
					end
					cardtopile(card,slotid)
					delay = 2
				end
				
				vars.mode_timer = vars.mode_timer + 1
				
				if (vars.mode_timer >= vars.deal) then
					vars.mode = 1
					vars.mode_timer = 0
				end
			end	
		elseif (delay == 0) then
			if (#piles.deck.cards > 0) and (#piles.hand.cards == 0) and ((#piles.pick1.cards == 0) or (#piles.pick2.cards == 0) or (#piles.pick3.cards == 0) or (#piles.pick4.cards == 0)) then
				local card = piles.deck.cards[#piles.deck.cards]
				
				for i=1,4 do
					local slotid = "pick" .. tostring(i)
					if (#piles[slotid].cards == 0) then
						inputsys.ignore = true
						delay = 3
						if (card.suit == 0) then
							spritesys.anim.set(card.sprite,"open")
						else
							spritesys.anim.set(card.sprite,"open2")
						end
						cardtopile(card,slotid)
						break
					end
				end	
			else
				inputsys.ignore = false
				
				if (piles.end1.need > 11) and (piles.end2.need > 11) and (piles.end3.need > 11) then
					victory()
				end
			end
			
			inputsys.ignore = false
		end
	end,
	
	mouse_left = function(key,data)
		local h = inputsys.gethover("pile")
		local deal = false
		for i,v in ipairs(h) do
			if (v.name == "deck") then
				deal = true
			end
		end
		
		if deal and (#piles.hand.cards == 0) and (delay == 0) and ((#piles.deck.cards > 0) or (#piles.pick1.cards > 0) or (#piles.pick2.cards > 0) or (#piles.pick3.cards > 0) or (#piles.pick4.cards > 0)) then
			vars.mode = 2
			vars.mode_timer = 0
			return
		end
		
		if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
			
			if pile.open or pile.open_pick then
				scripts.pick_pile(pile)
				delay = 5
			end
		end
		
		if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
			scripts.place_card()
			delay = 5
		end
	end,
	
	pick_pile = function(pile)
		local unit = targetcard
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		local blocks = {{}}
		local curr = pile.cards[1].suit
		local cblock = 1
		
		for i,v in ipairs(pile.cards) do
			if (v.suit == curr) then
				table.insert(blocks[#blocks], v)
			else
				curr = v.suit
				table.insert(blocks, {})
				table.insert(blocks[#blocks], v)
			end
			
			if (v.id == unit.id) then
				cblock = #blocks
			end
		end
		
		local doit = true
		--[[
		local size = #blocks[cblock] + 1
		for i=cblock,#blocks do
			if (size == #blocks[i] + 1) or (size == #blocks[i] - 1) then
				size = #blocks[i]
			else
				doit = false
				break
			end
		end
		]]--
		
		if doit then
			for i=cblock,#blocks do
				for j,u in ipairs(blocks[i]) do
					table.insert(these, u)
				end
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place or pile.open_pick then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do					
					cardtopile(v,targetpile)
				end
				
				if (pile.need ~= nil) then
					pile.need = pile.need + 2
					
					if (pile.number ~= nil) then
						spritesys.anim.set(pile.number.sprite,tostring(pile.need))
					end
				end
			end
		end
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local op = card.oldpile
		
		local u = pile.cards[#pile.cards]
		local v = 0
		local u_ = piles.hand.cards[1]
		local v_ = 0
		
		if (op ~= pile.name) and (pile.open_place == false) and (pile.open == false) then
			return false
		end
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			for i=0,#pile.cards-1 do
				local u2 = pile.cards[#pile.cards - i]
				if (u2.suit == u.suit) then
					v = v + 1
				else
					break
				end
			end
			
			for i=1,#piles.hand.cards do
				local u2_ = piles.hand.cards[i]
				if (u2_.suit == u_.suit) then
					v_ = v_ + 1
				else
					break
				end
			end
			
			if (v ~= v_ + 1) and (v ~= v_ - 1) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		local card = piles.hand.cards[1]
		local v = #piles.hand.cards
		local s = card.suit
		
		for i,k in ipairs(piles.hand.cards) do
			if (k.suit ~= s) then
				return false
			end
		end
		
		if (v < pile.need) or (pile.need > 11) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	deal = 0,
}

cheat_sprites =
{
	card =
	{
		file = "gfx/deck_cheat",
		w = 48,
		h = 80,
		default = "back",
		anim =
		{
			open = {0,0},
			back = {1,0},
			open2 = {2,0},
		},
	},
	number =
	{
		file = "gfx/cheat_numbers",
		w = 48,
		h = 80,
		default = "5",
		anim =
		{
			["5"] = {0,0},
			["7"] = {1,0},
			["9"] = {2,0},
			["11"] = {3,0},
			["13"] = {4,0},
		},
	},
}