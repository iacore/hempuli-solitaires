MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.add("lock","gfx/lock_clock",lock_sprites.lock)
		spritesys.add("info1","gfx/lock_info1")
		spritesys.add("info2","gfx/lock_info2")
		
		local origin = 32
		local y = 64
		local slotdata_end = {open = false, open_place = true, _anim = "pile_a", check_place = scripts.pile_check_place_end, locked = 0}
		piles.end1 = unitsys.create("pile","end1",origin + 56 * 8,y + 16 + 84 * 0,slotdata_end)
		slotdata_end._anim = "pile_b"
		piles.end2 = unitsys.create("pile","end2",origin + 56 * 8,y + 16 + 84 * 1,slotdata_end)
		slotdata_end._anim = "pile_c"
		piles.end3 = unitsys.create("pile","end3",origin + 56 * 8,y + 16 + 84 * 2,slotdata_end)
		slotdata_end._anim = "pile_d"
		piles.end4 = unitsys.create("pile","end4",origin + 56 * 8,y + 16 + 84 * 3,slotdata_end)
		
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place, locked = 0}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 56 * 8,y + 16,{open = false, card_offsets = {0,-0.5}, _visible = false})
		piles.slot1 = unitsys.create("pile","slot1",origin + 56 * 0,y,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 56 * 1,y,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 56 * 2,y,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 56 * 3,y,slotdata)
		piles.slot5 = unitsys.create("pile","slot5",origin + 56 * 4,y,slotdata)
		piles.slot6 = unitsys.create("pile","slot6",origin + 56 * 5,y,slotdata)
		piles.slot7 = unitsys.create("pile","slot7",origin + 56 * 6,y,slotdata)
		piles.slot8 = unitsys.create("pile","slot8",origin + 56 * 7,y,slotdata)
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","a13","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","b13","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","c13","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s, locked = 0, _sprites = {"lock"}})
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
	end,
	
	cleanup = function()
		unitsys.reset()
		spritesys.delete("lock")
		spritesys.delete("info1")
		spritesys.delete("info2")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Stack cards of every suit in $Ce3c188ascending order$CW from $Ca7d962A$CW to $Ca7d962K$CW on the slots on the right.",
		"- Cards are stacked in the $Ceb8edemiddle slots$CW in an alternating $Cc56f68red$CW/$C8bc8e7blue$CW pattern, either $Ce3c188ascending$CW or $Ce3c188descending$CW.",
		"- The direction of $Ce3c188ascension/descension$CW can change in the middle of a stack, so a placement of e.g. $Ca7d9628-9-8-7$CW is valid.$S$S$S$S$S$S$S$S$S",
		"- Every time you place a card anywhere, it $Ca4afb7locks$CW that slot for the next $Ca7d9622$CW moves. A $Ca4afb7locked$CW slot can't be interacted with.",
		"- Press $Ca7d962S$CW to autostack.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
		unitsys.create("info","info1",screenw * 0.5,screenh * 0.5 - 32,{_sprite = "info1", layer = l})
		unitsys.create("info","info2",screenw * 0.5,screenh * 0.5 + 108,{_sprite = "info2", layer = l})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					
					if (card.value > 1) then
						local slot = "slot" .. tostring(piletarget+1)
						spritesys.anim.set(card.sprite,card.name)
						cardtopile(card,slot)
						
						piletarget = (piletarget + 1) % 6
					else
						local slot = "end" .. tostring(card.suit)
						spritesys.anim.set(card.sprite,card.name)
						cardtopile(card,slot)
					end
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (delay == 0) then
			if (vars.autostack == 1) then
				scripts.autostack()
			else
				inputsys.ignore = false
				local cardcount = #piles.end1.cards + #piles.end2.cards + #piles.end3.cards + #piles.end4.cards
				
				if (cardcount == 52) then
					victory()
				elseif (cardcount == 50) then
					local unlock = false
					for i=1,8 do
						local id = "slot" .. tostring(i)
						local pile = piles[id]
						
						if (#pile.cards > 0) then
							for a,b in ipairs(pile.cards) do
								if (b.locked == 0) then
									unlock = true
									break
								end
							end
						end
						
						if unlock then
							break
						end
					end
					
					if (unlock == false) then
						scripts.lock()
						vars.autostack = 1
						inputsys.ignore = true
					end
				end
			end
		end
	end,
	
	autostack = function()
		vars.autostack = 0
		inputsys.ignore = false
		
		for i,unit in ipairs(UNITS) do
			if (unit.class == "card") and (delay == 0) and (#piles.hand.cards == 0) and (vars.mode == 1) and (#unit.pile > 0) and (unit.pile ~= "hand") and (unit.pile ~= "deck") and (unit.locked == 0) then
				local suitpilename = "end" .. tostring(unit.suit)
				local suitpile = piles[suitpilename]
				local pile = piles[unit.pile]
				
				if (unit.pileid == #pile.cards) and (unit.value == #suitpile.cards+1) and (suitpile.locked == 0) then
					inputsys.ignore = true
					vars.autostack = 1
					delay = 5
					cardtopile(unit,suitpilename)
					scripts.lock(suitpile)
					return
				end
			end
		end
	end,
	
	mouse_left = function(key,data)
		if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
						
			if pile.open or pile.open_pick then
				scripts.pick_card(unit)
				delay = 5
			end
		end
		
		if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
			scripts.place_card()
			delay = 5
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				local lock = false
				local c = piles.hand.cards[1]
				if (c.oldpile ~= targetpile) then
					lock = true
				end
				
				for i,v in ipairs(these) do					
					cardtopile(v,targetpile)
				end
				
				if lock then
					scripts.lock(pile)
				end
			end
		end
	end,
	
	lock = function(pile)
		for i,v in ipairs(UNITS) do
			if (v.locked ~= nil) and (v.locked > 0) then
				v.locked = v.locked + 1
				
				if (v.locked > 2) then
					if (v.class == "card") then
						spritesys.anim.set(v.sprites[1],"lock_3")
					end
					v.locked = 0
				elseif (v.locked == 2) and (v.class == "card") then
					spritesys.anim.set(v.sprites[1],"lock_2")
				end
			end
		end
		
		if (pile ~= nil) then
			pile.locked = 1
			
			for i,v in ipairs(pile.cards) do
				v.locked = 1
				spritesys.anim.set(v.sprites[1],"lock_1")
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit+3
		local v = card.value+1
		
		if (pile.locked > 0) then
			return false
		end
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			if (math.floor((c.suit-1)/2) == math.floor((s-1)/2)) or ((c.value ~= v-1) and (c.value ~= v+1)) then
				return false
			end
			
			s = c.suit
			v = c.value
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local s = card.suit
		local v = card.value
		local op = card.oldpile
		
		if (pile.locked > 0) or (card.locked > 0) then
			return false
		end
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			if (math.floor((c.suit-1)/2) == math.floor((s-1)/2)) or ((c.value ~= v-1) and (c.value ~= v+1)) then
				return false
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		
		local suitpilename = "end" .. tostring(card.suit)
		if (pile.name ~= suitpilename) then
			return false
		end
		
		if (#piles.hand.cards > 1) then
			return false
		end
		
		if (card.locked > 0) then
			return false
		end
		
		if (#pile.cards > 0) then
			local c = pile.cards[#pile.cards]
			
			if (c.value ~= v-1) or (c.suit ~= card.suit) then
				return false
			end
		elseif (card.value ~= 1) then
			return false
		end
		
		return true
	end,
}

MOD_VARS =
{
	autostack = 0,
}

lock_sprites =
{
	card =
	{
		file = "gfx/deck_lock",
		w = 48,
		h = 80,
		default = "back",
		anim =
		{
			back = {0,4},
			lock_1 = {1,4},
			lock_2 = {2,4},
			
			a1 = {0,0},
			a2 = {1,0},
			a3 = {2,0},
			a4 = {3,0},
			a5 = {4,0},
			a6 = {5,0},
			a7 = {6,0},
			a8 = {7,0},
			a9 = {8,0},
			a10 = {9,0},
			a11 = {10,0},
			a12 = {11,0},
			a13 = {12,0},
			
			b1 = {0,1},
			b2 = {1,1},
			b3 = {2,1},
			b4 = {3,1},
			b5 = {4,1},
			b6 = {5,1},
			b7 = {6,1},
			b8 = {7,1},
			b9 = {8,1},
			b10 = {9,1},
			b11 = {10,1},
			b12 = {11,1},
			b13 = {12,1},
			
			c1 = {0,2},
			c2 = {1,2},
			c3 = {2,2},
			c4 = {3,2},
			c5 = {4,2},
			c6 = {5,2},
			c7 = {6,2},
			c8 = {7,2},
			c9 = {8,2},
			c10 = {9,2},
			c11 = {10,2},
			c12 = {11,2},
			c13 = {12,2},
			
			d1 = {0,3},
			d2 = {1,3},
			d3 = {2,3},
			d4 = {3,3},
			d5 = {4,3},
			d6 = {5,3},
			d7 = {6,3},
			d8 = {7,3},
			d9 = {8,3},
			d10 = {9,3},
			d11 = {10,3},
			d12 = {11,3},
			d13 = {12,3},
		},
	},
	lock =
	{
		file = "gfx/lock_clock",
		w = 48,
		h = 80,
		default = "lock_3",
		anim =
		{
			lock_1 = {0,0},
			lock_2 = {1,0},
			lock_3 = {2,0},
		},
	},
}