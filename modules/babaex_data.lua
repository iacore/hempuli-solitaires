MOD_SCRIPTS =
{
	setup = function()
		piles = {}
		inputsys.ignore = true
		
		spritesys.change("card",{file="gfx/deck_babaex"})
		spritesys.change("pile",{file="gfx/pile_baba"})
		spritesys.add("frame","gfx/babaex_frame")
		
		local origin = 48
		local y = 64
		local slotdata_end = {open = false, open_place = true, _anim = "pile_a", check_place = scripts.pile_check_place_end}
		piles.end1 = unitsys.create("pile","end1",origin + 64 * 5,y + 88 * 0,slotdata_end)
		slotdata_end._anim = "pile_b"
		piles.end2 = unitsys.create("pile","end2",origin + 64 * 5,y + 88 * 1,slotdata_end)
		slotdata_end._anim = "pile_c"
		piles.end3 = unitsys.create("pile","end3",origin + 64 * 5,y + 88 * 2,slotdata_end)
		slotdata_end._anim = "pile_d"
		piles.end4 = unitsys.create("pile","end4",origin + 64 * 5,y + 88 * 3,slotdata_end)
		
		local slotdata_storage = {_anim = "pile_dark", check_place = scripts.pile_check_place_storage, card_offsets = {0,8}}
		piles.storage1 = unitsys.create("pile","storage1",origin + 64 * 1,y - 16,slotdata_storage)
		piles.storage2 = unitsys.create("pile","storage2",origin + 64 * 2,y - 16,slotdata_storage)
		piles.storage3 = unitsys.create("pile","storage3",origin + 64 * 3,y - 16,slotdata_storage)
		piles.storage4 = unitsys.create("pile","storage4",origin + 64 * 4,y - 16,slotdata_storage)
		
		y = y + 88
		local slotdata = {card_offsets = {0,12}, check_pick = scripts.pile_check_pick, check_place = scripts.pile_check_place}
		piles.hand = unitsys.create("pile","hand",0,0,{open = false, card_offsets = {0,12}, _visible = false, hold_offsets = {0,0}, lerp = 1.0})
		piles.deck = unitsys.create("pile","deck",origin + 64 * 0,y,{open = false, card_offsets = {0,-0.5}})
		piles.slot5 = unitsys.create("pile","slot5",origin + 64 * 0,y,slotdata)
		piles.slot1 = unitsys.create("pile","slot1",origin + 64 * 1,y,slotdata)
		piles.slot2 = unitsys.create("pile","slot2",origin + 64 * 2,y,slotdata)
		piles.slot3 = unitsys.create("pile","slot3",origin + 64 * 3,y,slotdata)
		piles.slot4 = unitsys.create("pile","slot4",origin + 64 * 4,y,slotdata)
		local cards = {"a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12","b1","b2","b3","b4","b5","b6","b7","b8","b9","b10","b11","b12","c1","c2","c3","c4","c5","c6","c7","c8","c9","c10","c11","c12","d1","d2","d3","d4","d5","d6","d7","d8","d9","d10","d11","d12","a13","b13","c13","d13"}
		local suits = {a = 1, b = 2, c = 3, d = 4}
		
		for i=1,#cards do
			local opt = math.random(1, #cards)
			if (i <= 4) then
				opt = #cards
			end
			
			local name = cards[opt]
			local s = suits[string.sub(name, 1, 1)]
			local v = tonumber(string.sub(name, 2))
			
			local card = unitsys.create("card",name,piles.deck.x,piles.deck.y,{value = v, suit = s, bvalue = v, bsuit = s, _sprites = {"frame"}, anysuit = false, onsame = false, ended = false})
			card.sprites[1].visible = false
			card.pile = "deck"
			table.insert(piles.deck.cards, card)
			card.pileid = #piles.deck.cards
			
			table.remove(cards, opt)
		end
	end,
	
	cleanup = function()
		unitsys.reset()
		spritesys.change("card",SPRITELIST.card)
		spritesys.change("pile",SPRITELIST.pile)
		spritesys.delete("babaex_frame")
	end,
	
	info = function(l)
		local text =
		{"RULES:",
		"- Pile number cards $Ca7d9621$CW to $Ca7d96210$CW of every suit in the $Ceb8edetop$CW slots.",
		"- The $Ceb8edemiddle$CW slots can hold $Ca7d9621$CW card each normally, and up to $Ca7d9623$CW if the cards form a rule (see below)",
		"- Number cards are stacked in $Ce3c188descending$CW order, but only stacks of $C74c0e6matching suit$CW can be moved together.",
		"- There are $Ca7d9623$CW types of $Ceb8ederule$CW cards:$S$C74c0e6Suit$CW - BABA, $C8faf62FOFO$CW, $Ce3c188JIJI$CW, $Ce37269KEKE$CW$S$C8faf62Verb$CW - Is$S$Ce3c188Quality$CW - $C74c0e6On Same$CW, $C74c0e6Any Suit$CW, $Cbc93deOpposite$CW, $Cbc93de+5$CW",
		"- $Ceb8edeRule$CW cards can be stacked by $Cbc93detype$CW.$S$Ceb8edeRule$CW cards can also stack to form $Ca7d9622$CW types of 3-card rules:$S1: $C74c0e6Suit$CW - $C8faf62Verb$CW - $Ce3c188Quality$CW (e.g. BABA - IS - $Cbc93de+5$CW)$S2: $C74c0e6Suit$CW - $C8faf62Verb$CW - $C74c0e6Suit$CW (e.g. $Ce37269KEKE$CW - IS - $Ce3c188JIJI$CW)",
		"- These $Ceb8ede3-card$CW rules can be placed in the $Ceb8edemiddle slots$CW.",
		"- When a $Ceb8ederule$CW exists, it affects the $C74c0e6suit$CW mentioned in the $Ceb8ederule$CW:$S$C74c0e6ON SAME$CW: the $C74c0e6suit's$CW cards can be placed on any cards of the $C74c0e6same suit$CW.$S$C74c0e6ANY SUIT$CW: the $C74c0e6suit's$CW cards behave as if they were of any $C74c0e6suit$CW.$S$Cbc93deOPPOSITE$CW: the $C74c0e6suit's$CW card values become their opposites (10 becomes 1 etc.)$S$Cbc93de+5$CW: the $C74c0e6suit's$CW card values increase by 5 (wrapping to 1 after 10).$S$C74c0e6SUIT 1$CW IS $C74c0e6SUIT 2$CW: the cards of $C74c0e6suit 1$CW temporarily become cards of $C74c0e6suit 2$CW.",
		"- Press $Ca7d962S$CW to autostack.$S",
		"NOTE: ANY SUIT doesn't affect stacking onto the $Ceb8edetop slots$CW.",
		}
		
		textsys.write(text,16,16,"info",{layer = l, font = "header", gap = 5, wrap = screenw - 32})
	end,
	
	gameflow = function()
		if (vars.mode == 0) then
			vars.mode_timer = vars.mode_timer + 1
			if (piletarget == nil) then
				piletarget = 0
			end
			
			if (vars.mode_timer > 30) and (vars.mode_timer % 3 == 0) then
				local pile = unitsys.find("pile","deck")
				
				if (#pile.cards > 0) then
					local card = pile.cards[#pile.cards]
					local slot = "slot" .. tostring(piletarget+1)
					spritesys.anim.set(card.sprite,card.name)
					cardtopile(card,slot)
					
					piletarget = (piletarget + 1) % 4
				end
			end
			
			if (#piles.deck.cards == 0) then
				vars.mode = 1
				vars.mode_timer = 1
				piletarget = nil
				inputsys.ignore = false
			end
		elseif (delay == 0) then
			if (vars.autostack == 1) then
				scripts.autostack()
			else
				inputsys.ignore = false
				
				if (#piles.end1.cards == 10) and (#piles.end2.cards == 10) and (#piles.end3.cards == 10) and (#piles.end4.cards == 10) then
					victory()
				end
			end
		end
	end,
	
	autostack = function()
		vars.autostack = 0
		inputsys.ignore = false
		
		for i,unit in ipairs(UNITS) do
			if (unit.class == "card") and (delay == 0) and (unit.bvalue < 11) and (unit.ended == false) and (#piles.hand.cards == 0) and (vars.mode == 1) and (#unit.pile > 0) and (unit.pile ~= "hand") and (unit.pile ~= "deck") then
				local suitpilename = "end" .. tostring(unit.suit)
				local suitpile = piles[suitpilename]
				local pile = piles[unit.pile]
				
				if (unit.pileid == #pile.cards) and (unit.value == #suitpile.cards+1) then
					inputsys.ignore = true
					vars.autostack = 1
					delay = 5
					cardtopile(unit,suitpilename)
					unit.ended = true
					return
				end
			end
		end
	end,
	
	mouse_left = function(key,data)
		if (targetcard ~= nil) and (#piles.hand.cards == 0) and (delay == 0) then
			local unit = targetcard
			local pile = unitsys.find("pile",unit.pile)
						
			if pile.open or pile.open_pick then
				scripts.pick_card(unit)
				delay = 5
				
				scripts.rules()
			end
		end
		
		if (targetpile ~= nil) and (#piles.hand.cards > 0) and (delay == 0) then
			scripts.place_card()
			delay = 5
			
			scripts.rules()
		end
	end,
	
	rules = function()
		vars.trans1 = 0
		vars.trans2 = 0
		vars.trans3 = 0
		vars.trans4 = 0
		vars.rule1 = 0
		vars.rule2 = 0
		vars.rule3 = 0
		vars.rule4 = 0
		
		for i,u1 in ipairs(UNITS) do
			if (u1.class == "card") then
				if (u1.value < 11) and (u1.ended == false) then
					spritesys.anim.set(u1.sprite,u1.name)
					u1.sprites[1].visible = false
					u1.onsame = false
					u1.anysuit = false
					u1.value = u1.bvalue
					u1.suit = u1.bsuit
				end
				
				if (u1.value == 13) then
					local pile = piles[u1.pile]
					local u2 = pile.cards[u1.pileid+1]
					local u3 = pile.cards[u1.pileid+2]
					
					if (u2 ~= nil) and (u3 ~= nil) and (u2.value == 12) then
						if (u3.value == 13) then
							local id = "trans" .. tostring(u1.suit)
							vars[id] = u3.suit
						elseif (u3.value == 11) then
							local id = "rule" .. tostring(u1.suit)
							vars[id] = u3.suit
						end
					end
				end
			end
		end
		
		local trans = {0, 0, 0, 0}
		local suits = {"a", "b", "c", "d"}
		
		for i=1,4 do
			local id = "trans" .. tostring(i)
			if (vars[id] > 0) then
				trans[i] = vars[id]
				for j=1,4 do
					local id2 = "trans" .. tostring(j)
					if (vars[id2] > 0) then
						trans[i] = vars[id2]
						for k=1,4 do
							local id3 = "trans" .. tostring(k)
							if (vars[id3] > 0) then
								trans[i] = vars[id3]
							end
						end
					end
				end
			end
		end
		
		for i,u in ipairs(UNITS) do
			if (u.class == "card") and (u.value < 11) then
				local s = u.suit
				local t = trans[s]
				
				if (t > 0) then
					u.suit = t
				end
				
				local id = "rule" .. tostring(u.suit)
				local rule = vars[id]
				
				if (rule > 0) then
					if (rule == 1) then
						u.value = (u.value + 4) % 10 + 1
					elseif (rule == 2) then
						u.anysuit = true
					elseif (rule == 3) then
						u.onsame = true
					elseif (rule == 4) then
						u.value = 10 - (u.value - 1)
					end
				end
				
				if ((u.value ~= u.bvalue) or (u.suit ~= u.bsuit)) and (u.ended == false) then
					local sname = suits[u.suit]
					local id = sname .. tostring(u.value)
					spritesys.anim.set(u.sprite,id)
					u.sprites[1].visible = true
				end
			end
		end
	end,
	
	pick_card = function(unit)
		local pile = unitsys.find("pile",unit.pile)
		local these = {}
		local mx,my = love.mouse.getPosition()
		mx = math.floor(mx * 0.5)
		my = math.floor(my * 0.5)
		piles.hand.hold_offsets[1] = mx - unit.x
		piles.hand.hold_offsets[2] = my - unit.y
		
		if (pile.check_pick == nil) or pile.check_pick(pile,unit) then
			for i=unit.pileid,#pile.cards do
				local u = pile.cards[i]
				table.insert(these, u)
			end
			
			for i,v in ipairs(these) do
				cardtopile(v,"hand")
			end
		end
	end,
	
	place_card = function()
		local pile = piles[targetpile]
		local these = {}
		
		if pile.open or pile.open_place then
			if (pile.check_place == nil) or pile.check_place(pile) then
				for i,u in ipairs(piles.hand.cards) do
					table.insert(these, u)
				end
				
				for i,v in ipairs(these) do
					cardtopile(v,targetpile)
				end
			end
		end
	end,
	
	pile_check_pick = function(pile,card)
		local s = card.suit
		local v = card.value
		local v_ = card.value
		local same = false
		local nomore = false
		local anysuit = card.anysuit
		local suits = {}
		local scount = 0
		
		for i=card.pileid,#pile.cards do
			local c = pile.cards[i]
			
			if (v_ < 11) then
				if (c.value > 10) then
					return false
				end
				
				if (c.value ~= v) or ((c.suit ~= s) and (c.anysuit == false) and (anysuit == false)) then
					return false
				end
				
				if (suits[c.suit] == nil) then
					suits[c.suit] = 1
					scount = scount + 1
				end
				
				if (scount > 2) then
					return false
				end
			else
				if nomore then
					return false
				end
				
				if (c.value < 11) then
					return false
				end
				
				if (c.value == v_) and (i > card.pileid) then
					same = true
				end
				
				if (c.value ~= v_) and same then
					return false
				end
				
				if (i > card.pileid) and (same == false) and (c.value == 13) then
					nomore = true
				end
				
				if (c.value ~= v) and ((c.value ~= 13) or (v ~= 11)) and (same == false) then
					return false
				end
			end
			
			anysuit = c.anysuit
			v = v - 1
			s = c.suit
		end
		
		return true
	end,
	
	pile_check_place = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		local op = card.oldpile
		local diff = false
		local same = false
		
		if (#pile.cards > 0) and (op ~= pile.name) then
			local c = pile.cards[#pile.cards]
			
			if (v < 11) then
				if (c.value > 10) then
					return false
				end
				
				if (c.value ~= v+1) and ((card.onsame == false) or ((card.suit ~= c.suit) and (c.anysuit == false) and (card.anysuit == false))) then
					return false
				end
			else
				if (c.value < 11) then
					return false
				end
				
				if (c.value ~= v) and (c.value ~= v+1) and ((c.value ~= 12) or (v ~= 13)) then
					return false
				end
			end
		end
		
		return true
	end,
	
	pile_check_place_storage = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		local op = card.oldpile
		
		if (v < 11) then
			if (#piles.hand.cards > 1) or (#pile.cards > 0) then
				return false
			end
		elseif (#piles.hand.cards > 1) or (#pile.cards > 0) then
			if (#piles.hand.cards + #pile.cards > 3) then
				return false
			end
			
			local test = {}
			for a,b in ipairs(pile.cards) do
				table.insert(test, b.value)
			end
			for a,b in ipairs(piles.hand.cards) do
				table.insert(test, b.value)
			end
			
			local sv = 13
			
			for a,b in ipairs(test) do
				if (b == 13) and (a == 3) then
					sv = 13
				end
				
				if (b ~= sv) then
					return false
				end
				
				sv = sv - 1
			end
		end
		
		return true
	end,
	
	pile_check_place_end = function(pile)
		local card = piles.hand.cards[1]
		local v = card.value
		
		local suitpilename = "end" .. tostring(card.suit)
		if (pile.name ~= suitpilename) then
			return false
		end
		
		if (#piles.hand.cards > 1) then
			return false
		end
		
		if (card.value > 10) then
			return false
		end
		
		if (#pile.cards > 0) then
			local c = pile.cards[#pile.cards]
			
			if (c.value ~= v-1) or (c.suit ~= card.suit) then
				return false
			end
		elseif (card.value ~= 1) then
			return false
		end
		
		card.ended = true
		
		return true
	end,
}

MOD_VARS =
{
	autostack = 0,
	trans1 = 0,
	trans2 = 0,
	trans3 = 0,
	trans4 = 0,
	rule1 = 0,
	rule2 = 0,
	rule3 = 0,
	rule4 = 0,
}