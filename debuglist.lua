-- v.0.1.0
debuglist = {}
debugline = ""

function draw_debuglist()
	for i,v in ipairs(debuglist) do
		if (v[2] > 0) then
			love.graphics.print(tostring(v[1]), 5, (i) * 24)
			
			v[2] = v[2] - 1
		elseif (v[2] == 0) then
			table.remove(debuglist, i)
		end
	end
end

function debugline_draw()
	if (debugline ~= nil) then
		love.graphics.print(tostring(debugline), 5, 5)
	else
		debugline = ""
	end
end

function alert(text,duration_)
	local duration = duration_ or 360
	
	table.insert(debuglist, {text, duration})
end

function clearalerts()
	debuglist = {}
end