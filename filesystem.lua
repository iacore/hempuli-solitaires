-- v.0.1.1
filesys =
{
	loadlist = function(name,appdata_)
		local result = {}
		local filename = filesys.getdir() .. "/" .. name
		local appdata = appdata_ or false
		
		-- alert(filename .. ", " .. tostring(filesys.exists(filename)))
		
		if (appdata == false) then
			if filesys.exists(filename) then
				for line in io.lines(filename) do
					table.insert(result, line)
				end
			end
		else
			if love.filesystem.getInfo(name,"file") then
				for line in love.filesystem.lines(name) do
					table.insert(result, line)
				end
			end
		end
		
		return result
	end,
	
	savelist = function(name,data,appdata_)
		local filename = filesys.getdir() .. "/" .. name
		local appdata = appdata_ or false
		
		local datastring = ""
		
		if (#data > 1) then
			for i,v in ipairs(data) do
				datastring = datastring .. v .. "\n"
			end
		elseif (#data == 1) then
			datastring = data[1]
		end
		
		if (appdata == false) then
			local file = io.open(filename, "w")
			io.output(file)
			io.write(datastring)
			io.close(file)
		else
			love.filesystem.write(name,datastring)
		end
	end,
	
	getdir = function()
		local result = love.filesystem.getSourceBaseDirectory()
		
		if filesys.exists(result .. "/main.lua") then
			result = result .. "/.."
		end
		
		return result
	end,
	
	parse = function(text,rule_)
		local result = {}
		local rule = rule_ or "%S+"
		
		for clip in string.gmatch(text,rule) do
			table.insert(result, clip)
		end
		
		return result
	end,
	
	exists = function(name)
		local file = io.open(name)
		
		if (file ~= nil) then
			io.close(file)
			return true
		end
		
		return false
	end,
	
	ini =
	{	
		open = function(name)
			local data = filesys.loadlist(name,true)
			local ini = {}
			setmetatable(ini, filesys.ini)
			
			ini.file = name
			local group = ""
			local success = true
			
			for i,v in ipairs(data) do
				local eq = string.find(v, "=")
				
				if (string.sub(v, 1, 1) == "[") and (string.sub(v, -1) == "]") then
					group = string.sub(v, 2, string.len(v) - 1)
					ini[group] = {}
				elseif (eq ~= nil) then
					local item = string.sub(v, 1, eq - 1)
					local val = string.sub(v, eq + 1)
					
					if (#group > 0) and (ini[group] ~= nil) then
						ini[group][item] = val
					else
						success = false
						break
					end
				else
					success = false
					break
				end
			end
			
			if (#data == 0) then
				success = false
			end
			
			return ini,success
		end,
		
		close = function(file)
			file = {}
			collectgarbage()
		end,
		
		read = function(file,group,item)
			if (file[group] ~= nil) then
				return file[group][item]
			end
			
			return nil
		end,
		
		write = function(file,group,item,val)
			if (file[group] == nil) then
				file[group] = {}
			end
			
			file[group][item] = tostring(val)
		end,
		
		update = function(file,group,item,val)
			filesys.ini.write(file,group,item,val)
			filesys.ini.store(file,file.file,false)
		end,
		
		store = function(file,name,closeafter_)
			local ca = true
			if (closeafter_ ~= nil) then
				ca = closeafter_
			end
			
			local data = {}
			for i,gdata in pairs(file) do
				if (type(gdata) == "table") then
					local group = "[" .. i .. "]"
					table.insert(data, group)
					
					for a,val in pairs(gdata) do
						local item = a .. "=" .. tostring(val)
						table.insert(data, item)
					end
				end
			end
			
			filesys.savelist(name,data,true)
			
			if ca then
				filesys.ini.close(file)
			end
		end,
	}
}

-- T�m� asettaa jutun niin, ett� filesys.ini.open()illa luodut init voivat accessoida filesys.inin funktioita helposti
filesys.ini.__index = filesys.ini