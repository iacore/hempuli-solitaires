menulist =
{
	main =
	{
		enter = function()
			local save = filesys.ini.open("save.txt")
			local thumb = unitsys.create("thumbnail","thumb",screenw * 0.5,screenh * 0.5 - 88)
			
			local icons = {"babataire","babaex","eldritch","lock","poker","wolf","thing","cheat"}
			for i,v in ipairs(icons) do
				local x = 32 + (i-1) * 56
				local y = screenh - 48
				
				local icon = unitsys.create("icon",v,x,y,{_anim = v, oy = y})
				
				local data = subgames[v] or {}
				icon.title = data.title or "NONE"
				icon.titlecol = data.title_colour or "ffffff"
				icon.desc = data.desc or "Text redacted."
				icon.rating = data.rating or "´´´´´"
				icon.deck = data.deck or "-"
				icon.back = data.back_colour or 0x3f547c
				icon.wins = save:read("wins",v) or "0"
				icon.mstreak = save:read("mstreak",v) or "0"
			end
			
			save:store("save.txt")
		end,
		
		update = function()
		end,
		
		input = "main",
		
		structure =
		{
			
		},
	},
	ingame =
	{
		--[[
		enter_submenu = function()
			inputsys.set(inputlist.ingame)
		end,
		]]--
		
		input = "ingame",
		
		update = function()
			if (vars.victory == 0) then
				local otc = targetcard
				targetcard = nil
				targetpile = nil
				local h = inputsys.gethover()
				local highest = 0
				
				for i,unit in ipairs(h) do
					if (unit.class == "card") and (unit.pile ~= "hand") then
						if (unit.zlayer > highest) then
							highest = unit.zlayer
							targetcard = unit
							targetpile = unit.pile
						end
					end
					
					if (unit.class == "pile") and (unit.name ~= "hand") and (targetpile == nil) and (unit.open or unit.open_place or unit.open_pick) then
						targetpile = unit.name
					end
				end
				
				for i,unit in ipairs(UNITS) do
					if (unit.class == "card") then
						if (#unit.pile > 0) and (unit.pileid > 0) then
							local pile = unitsys.find("pile",unit.pile)
							local ox,oy = pile.card_offsets[1],pile.card_offsets[2]
							
							local hox,hoy = 0,0
							local lerp = 0.2
							if (unit.pile == "hand") then
								hox = 0 - piles.hand.hold_offsets[1]
								hoy = 0 - piles.hand.hold_offsets[2]
								lerp = 0.9
							end
							unit.lerp = lerp
							
							if (pile ~= nil) then
								unit.x = pile.x + (unit.pileid-1) * ox + hox
								unit.y = pile.y + (unit.pileid-1) * oy + hoy
							end
						end
						
						if (scripts.card_pos ~= nil) then
							scripts.card_pos(unit)
						end
					end
				end
				
				if (scripts.gameflow ~= nil) then
					scripts.gameflow()
				end
			end
			
			local mx,my = love.mouse.getPosition()
			piles.hand.x = mx / scaling
			piles.hand.y = my / scaling
		end,
		
		draw = function()
			if (vars.victory == 1) and (scripts.victoryeffect ~= nil) then
				scripts.victoryeffect()
			end
		end,
		
		leave = function()
			vars.victory = 0
		end,
		
		structure =
		{
			
		},
	},
	info =
	{
		input = "info",
		
		enter = function()
			menusys.button.disable("ui",false)
			
			ORDER[#ORDER + 1] = {}
			local o = ORDER[#ORDER]
			local c = getrgb(BACKCOL)
			o.effect = function() love.graphics.setColor(c) love.graphics.rectangle("fill", 0, 0, screenw * scaling, screenh * scaling) love.graphics.setColor(1, 1, 1, 1) end
			menusys.button.create("closeinfo",screenw - 20,20,"info",{_sprite = "button", _anim = "close", w = 24, h = 24, layer = #ORDER})
			
			scripts.info(#ORDER)
		end,
	},
}