function love.load()
	require("gamesystem")
	require("scriptsystem")
	require("inputsystem")
	require("input")
	require("unitsystem")
	require("tools")
	require("render")
	require("debuglist")
	require("spritesystem")
	require("textsystem")
	require("filesystem")
	require("menusystem")
	require("menu")
	require("undo")
	require("values")
	
	setup()
end

function setup()
	UNITS = {}
	camera = {}
	
	random_seed = os.time()
	math.randomseed(random_seed)
	
	game = ""
	movecamera = true
	delay = 0
	
	local fontfile = "Inconsolata.ttf"
	local fontdata =
	{
		small = {fontfile, 12},
		medium = {fontfile, 24},
		large = {fontfile, 36},
	}
	textsys.font.init(fontdata)
	textsys.font.set("medium")
	
	gamesys.init()
	
	c_map = love.graphics.newCanvas()
	setup_camera()
	spritesys.init()
	scriptsys.init()
	textsys.init("default")
	
	local startmenu = GAME.startmenu or "main"
	menusys.init(startmenu)
end

function startgame(name)
	if (scripts.cleanup ~= nil) then
		scripts.cleanup()
	else
		menusys.button.erase()
		textsys.erase()
		unitsys.reset()
		spritesys.init()
	end
	
	local save = filesys.ini.open("save.txt")
	local wins = save:read("wins",name) or "0"
	local streak = save:read("streak",name) or "0"
	save:close()
	textsys.erase("wincount")
	textsys.write("WINS: " .. wins,screenw - 6,36,"wincount",{font = "header", xoffset = -1.0})
	textsys.write("STREAK: " .. streak,screenw - 6,36 + 14,"wincount",{font = "header", xoffset = -1.0})
	
	vars.victory = 0
	scriptsys.setup(name)
	scripts.setup()
end

function clear()
	UNITS = {}
	ORDER = {}
	unit_id = 0
	setup_camera()
end

function setup_camera()
	camera = {}
	camera.x = 0
	camera.y = 0
	camera.w = screenw
	camera.h = screenh
	camera.midx = camera.w * 0.5
	camera.midy = camera.h * 0.5
end

function victory()
	vars.victory = 1
	vars.mode_timer = 0
	
	addwin(game)
	
	local save = filesys.ini.open("save.txt")
	local wins = save:read("wins",game) or "0"
	local streak = save:read("streak",game) or "0"
	save:close()
	textsys.erase("wincount")
	textsys.write("WINS: " .. wins,screenw - 6,36,"wincount",{font = "header", xoffset = -1.0})
	textsys.write("STREAK: " .. streak,screenw - 6,36 + 14,"wincount",{font = "header", xoffset = -1.0})
	
	if (scripts.victory ~= nil) then
		scripts.victory()
	else
		ORDER[#ORDER + 1] = {}
		local o = ORDER[#ORDER]
		local c = getrgb(BACKCOL,1.5)
		o.effect = function() love.graphics.setColor(c) love.graphics.rectangle("fill", 0, math.floor((screenh * 0.5 - 48)) * scaling, screenw * scaling, 96 * scaling) love.graphics.setColor(1, 1, 1, 1) end
		textsys.write("VICTORY",screenw * 0.5,screenh * 0.5,"wincount",{font = "serif", xoffset = -0.5, yoffset = -0.5, layer = #ORDER})
	end
end

function addwin(name)
	local save = filesys.ini.open("save.txt")
	local wins = save:read("wins",name) or "0"
	local streak = save:read("streak",name) or "0"
	local mstreak = save:read("mstreak",name) or "0"
	wins = tonumber(wins) + 1
	streak = tonumber(streak) + 1
	mstreak = math.max(tonumber(mstreak), streak)
	save:update("wins",name,tostring(wins))
	save:update("streak",name,tostring(streak))
	save:update("mstreak",name,tostring(mstreak))
	save:close()
end

function addloss(name)
	local save = filesys.ini.open("save.txt")
	local wins = save:read("wins",game) or "0"
	local streak = "0"
	save:update("streak",name,tostring(streak))
	save:close()
	
	textsys.erase("wincount")
	textsys.write("WINS: " .. wins,screenw - 6,36,"wincount",{font = "header", xoffset = -1.0})
	textsys.write("STREAK: " .. streak,screenw - 6,36 + 14,"wincount",{font = "header", xoffset = -1.0})
end

function camera_update()
	camera.midx = camera.w * 0.5
	camera.midy = camera.h * 0.5
	camera.x = screenw * 0.5 - camera.w * 0.5
	camera.y = screenh * 0.5 - camera.h * 0.5
	camera.rightx = camera.x + camera.w
	camera.bottomy = camera.y + camera.h
end

function love.update(delta)
	love.window.setTitle(TITLE .. " (" .. tostring(love.timer.getFPS()) .. ")")
	
	if love.mouse.isDown(1) then
		inputsys.mouse_isdown(1)
	end
	
	if love.mouse.isDown(2) then
		inputsys.mouse_isdown(2)
	end
	
	menusys.update()
	update_pos()
	spritesys.anim.update()
	
	if (delay > 0) then
		delay = delay - 1
	end
end

function love.quit()
	--[[
	if (#game > 0) and (vars.victory == 0) then
		local save = filesys.ini.open("save.txt")
		save:write("streak",game,"0")
		save:store("save.txt")
	end
	]]--
end