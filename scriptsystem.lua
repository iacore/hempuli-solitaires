-- v.0.1.0
scriptsys =
{
	init = function()
		scripts = {}
		vars = {}
		
		for i,name in pairs(SCRIPT_MODS) do
			local fname = name .. "_data"
			require("modules/" .. fname)
			local sname = "MOD_SCRIPTS"
			local vname = "MOD_VARS"
			
			SCRIPTS[name] = {}
			VARS[name] = {}
			
			for a,b in pairs(_G[sname]) do
				SCRIPTS[name][a] = b
			end
			
			for a,b in pairs(_G[vname]) do
				VARS[name][a] = b
			end
			
			_G[sname] = nil
			_G[vname] = nil
		end
		
		scriptsys.setup()
	end,
	
	setup = function(name)
		scripts = SCRIPTS[name] or {}
		vars = {}
		
		if (VARS.GLOBAL ~= nil) then
			for i,v in pairs(VARS.GLOBAL) do
				if (type(v) == "table") then
					vars[i] = copytable(v)
				else
					vars[i] = v
				end
			end
		end
		
		if (VARS[name] ~= nil) then
			for i,v in pairs(VARS[name]) do
				if (type(v) == "table") then
					vars[i] = copytable(v)
				else
					vars[i] = v
				end
			end
		end
	end,
}

scripts = {}
vars = {}

SCRIPT_MODS = {}
SCRIPTS = {}
VARS = {}