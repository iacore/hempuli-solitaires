function love.draw()
	love.graphics.setColor(1, 1, 1, 1)
	
	draw_clear()
	love.graphics.setBlendMode("alpha")
	love.graphics.setColor(1, 1, 1, 1)
	spritesys.canvas.draw(c_map)
	
	textsys.draw(0)
	menusys.button.draw(0)
	for i,layer in ipairs(ORDER) do
		if (layer.effect ~= nil) then
			layer.effect(i)
		end
		
		draw_units(layer)
		textsys.draw(i)
		menusys.button.draw(i)
	end
	
	spritesys.canvas.clear()
	
	love.graphics.setColor(1, 1, 1, 1)
	menusys.draw()
	draw_debuglist()
end

function draw_clear()
	local c = getrgb(BACKCOL)
	love.graphics.clear(c)
end

function draw_clear_transparent()
	love.graphics.clear(0,0,0,0)
end

function draw_units(list)
	love.graphics.setColor(1, 1, 1, 1)
	
	for i,unit in ipairs(list) do
		spritesys.draw(unit.sprite,camera.x + unit.xp,camera.y + unit.yp)
		
		if (unit.sprites ~= nil) then
			spritesys.drawmany(unit.sprites,camera.x + unit.xp,camera.y + unit.yp)
		end
	end
end