-- v.0.1.4
spritesys =
{
	init = function()
		spritesys.image.empty()
		IMAGES = {}
		SPRITES = {}
		
		spritesys.image.build()
		spritesys.build()
	end,
	
	build = function()
		SPRITES = {}
		
		for i,v in pairs(SPRITELIST) do
			local file = v[1] or v.file or i
			spritesys.add(i,file,v)
		end
	end,
	
	image =
	{
		empty = function()
			for i,v in pairs(IMAGES) do
				spritesys.image.delete(i)
			end
			
			IMAGES = {}
		end,
		
		build = function()
			for i,v in pairs(SPRITELIST) do
				local file = v[1] or v.file or i
				
				if (IMAGES[file] == nil) then
					IMAGES[file] = love.graphics.newImage(file ..".png")
				end
			end
		end,
		
		change = function(file,new)
			local img = IMAGES[file]
			if (img ~= nil) then
				img:release()
				IMAGES[file] = nil
			end
			
			IMAGES[new] = love.graphics.newImage(new ..".png")
		end,
		
		delete = function(file)
			local img = IMAGES[file]
			if (img ~= nil) then
				img:release()
			end
			
			IMAGES[file] = nil
		end,
		
		add = function(file)
			if (IMAGES[file] == nil) then
				IMAGES[file] = love.graphics.newImage(file ..".png")
			end
		end,
		
		clean = function(img)
			for i,v in pairs(SPRITES) do
				if (v.file == img) then
					return
				end
			end
			
			spritesys.image.delete(img)
		end,
	},
	
	add = function(name,file,data_)
		SPRITES[name] = {}
		local data = data_ or {}
		local d = SPRITES[name]
		d.file = file or name
		d.name = name
		if (IMAGES[d.file] == nil) then
			spritesys.image.add(d.file)
		end
		d.image = IMAGES[d.file]
		d.w = data.w or d.image:getWidth()
		d.h = data.h or d.image:getHeight()
		d.xoffset = data.xoffset or 0
		d.yoffset = data.yoffset or 0
		
		spritesys.anim.setup(name,data.anim,data.default)
	end,
	
	change = function(name,data)
		local d = SPRITES[name]
		if (d ~= nil) then
			if (data.file ~= nil) and (data.file ~= d.file) then
				local old = d.file
				local new = data.file
				d.file = data.file
				
				if (IMAGES[data.file] == nil) then
					if (data.replace ~= nil) and data.replace then
						spritesys.image.change(old,new)
					else
						spritesys.image.add(new)
					end
				end
				
				d.image = IMAGES[d.file]
			end
			
			d.w = data.w or d.w
			d.h = data.h or d.h
			d.xoffset = data.xoffset or d.xoffset
			d.yoffset = data.yoffset or d.yoffset
			
			if (data.anim ~= nil) then
				local default = data.default or d.default
				spritesys.anim.setup(name,data.anim,default)
			end
		end
	end,
	
	delete = function(name)
		if (SPRITES[name] ~= nil) then
			local file = SPRITES[name].file
			SPRITES[name] = nil
			spritesys.image.clean(file)
		end
	end,
	
	drawmany = function(sprites,x,y,r_)
		for i,sprite in ipairs(sprites) do
			spritesys.draw(sprite,x,y,r_)
		end
	end,
	
	draw = function(sprite,x,y,r_)
		if (sprite.frame ~= nil) and sprite.visible then
			local frame = sprite.frame
			local name = sprite.anim
			local data = spritesys.get(sprite,name)
			local r = r_ or sprite.angle or 0
			
			if (data ~= nil) and (data[frame] ~= nil) then
				local rx = (x + sprite.data.xoffset)
				local ry = (y + sprite.data.yoffset)
				if integral then
					rx = math.floor(rx)
					ry = math.floor(ry)
				end
				rx = rx * scaling
				ry = ry * scaling
				
				love.graphics.draw(sprite.data.image, data[frame], rx, ry, r, scaling, scaling, sprite.data.w * 0.5, sprite.data.h * 0.5)
			end
		end
	end,
	
	draw_manual = function(name,anim_,frame_,x,y,mult_,maxsize_,override,extra_)
		if (SPRITES[name] ~= nil) then
			local sprite = SPRITES[name]
			local extra = extra_ or {}
			
			local anim = anim_ or sprite.default
			local frame = frame_ or 1
			local mult = mult_ or scaling
			local xoffset = extra.xoffset or sprite.xoffset
			local yoffset = extra.yoffset or sprite.yoffset
			local w = sprite.w
			local h = sprite.h
			local angle = extra.angle or sprite.angle or 0
			local xo = extra.xo
			local yo = extra.yo
			local image = sprite.image
			
			local maxsize = maxsize_ or 0
			if (maxsize > 0) then
				local sizelimit = math.max(sprite.w, sprite.h)
				
				if (sizelimit > maxsize) then
					mult = maxsize / sizelimit
				end
			end
			
			if (override ~= nil) then
				xoffset = override.xoffset or xoffset
				yoffset = override.yoffset or yoffset
				w = override.w or w
				h = override.h or h
				image = sprite.image or image
			end
			
			-- alert(name .. ", " .. anim .. ", " .. tostring(sprite.anim[anim]) .. ", " .. tostring(#sprite.anim[anim]))
			local x_,y_ = x,y
			if integral then
				x_ = math.floor(x_)
				y_ = math.floor(y_)
			end
			x_ = x_ * scaling
			y_ = y_ * scaling
			
			if (sprite.anim ~= nil) and (sprite.anim[anim] ~= nil) and (sprite.anim[anim][frame] ~= nil) then
				love.graphics.draw(image, sprite.anim[anim][frame], x - w * 0.5 * mult + xoffset * mult, y - h * 0.5 * mult + yoffset * mult, angle, mult, nil, xo, yo)
			end
		end
	end,
	
	get = function(obj,name)
		if (obj.anim ~= nil) and (obj.data ~= nil) then
			return obj.data.anim[name] or {}
		else
			return obj.sprite.data.anim[name] or {}
		end
	end,
	
	set = function(unit,name,anim,visible_)
		local sprite = {}
		
		if (SPRITES[name] ~= nil) then
			local data = SPRITES[name]
			sprite.name = name
			sprite.data = SPRITES[name]
			sprite.anim = anim or sprite.data.default
			sprite.timer = 0
			sprite.frame = 1
			sprite.duration = 15
			sprite.update = false
			sprite.angle = data.angle or 0
			sprite.w = data.w
			sprite.h = data.h
			sprite.owner = unit.id
			sprite.visible = true
			
			if (visible_ ~= nil) then
				sprite.visible = visible_
			end
			
			if (data.anim[sprite.anim] == nil) then
				error(sprite.name .. ", " .. sprite.anim)
			end
			
			if (data.anim[sprite.anim].frames > 1) then
				sprite.update = true
			end
		end
		
		return sprite
	end,
	
	zlayer =
	{
		build = function()
			ORDER = {}
			
			for i,unit in ipairs(UNITS) do
				spritesys.zlayer.add(unit)
			end
		end,
		
		update = function()
			for a,layer in pairs(ORDER) do
				for i,unit in ipairs(layer) do
					unit.zlayer = i-1
				end
			end
		end,
		
		add = function(unit)
			if (unit.sprite ~= nil) then
				local z = unit.zlayer or 999
				local l = unit.layer or 1
				
				if (ORDER[l] == nil) then
					ORDER[l] = {}
				end
				
				if (#ORDER[l] == 0) then
					table.insert(ORDER[l], unit)
				else
					local limit = #ORDER[l]
					
					for a,b in ipairs(ORDER[l]) do
						local cz = b.zlayer
						
						if (z < cz) and (a <= limit) then
							table.insert(ORDER[l], a, unit)
							break
						end
						
						if (z >= cz) and (a == limit) then
							table.insert(ORDER[l], unit)
							break
						end
					end
				end
			end
			
			spritesys.zlayer.update()
		end,
		
		remove = function(unit)
			for a,layer in pairs(ORDER) do
				for i,v in ipairs(layer) do
					if (v.id == unit.id) then
						table.remove(layer, i)
						return
					end
				end
				
				if (#layer == 0) and (a > 1) then
					ORDER[a] = nil
				end
			end
		end,
		
		tofront = function(unit)
			spritesys.zlayer.remove(unit)
			if (ORDER[unit.layer] == nil) then
				ORDER[unit.layer] = {}
			end
			
			unit.zlayer = #ORDER[unit.layer]
			spritesys.zlayer.add(unit)
		end,
		
		toback = function(unit)
			spritesys.zlayer.remove(unit)
			unit.zlayer = 0
			spritesys.zlayer.add(unit)
		end,
		
		setlayer = function(unit,l)
			spritesys.zlayer.remove(unit)
			unit.layer = l
			spritesys.zlayer.add(unit)
		end,
	},
	
	canvas =
	{
		
		render = function()
			love.graphics.setColor(1, 1, 1, 1)
			
			if (RENDER.funcs ~= nil) then
				for i,func in ipairs(RENDER.funcs) do
					func(RENDER.list)
				end
			end
		end,
		
		handle = function(target,funcs,list)
			RENDER = {}
			RENDER.list = list
			RENDER.funcs = funcs or {}
			
			love.graphics.setColor(1, 1, 1, 1)
			target:renderTo(spritesys.canvas.render)
		end,
		
		draw = function(canvas,stencil,shader)
			if (stencil ~= nil) then
				love.graphics.setStencilTest(stencil[1],stencil[2])
			else
				love.graphics.setStencilTest()
			end
			
			love.graphics.setShader(shader)
			love.graphics.draw(canvas)
		end,
		
		clear = function()
			love.graphics.setStencilTest()
			love.graphics.setShader()
			RENDER = {}
		end,
	},
	
	anim =
	{
		setup = function(name,anims,default)
			local d = SPRITES[name]
			
			if (d ~= nil) then
				d.anim = {}
				d.default = default or "default"
				
				if (anims ~= nil) and (anims[d.default] == nil) then
					d.default = nil
				end
				
				if (anims == nil) then
					d.anim.default = {}
					d.anim.default[1] = love.graphics.newQuad(0, 0, d.w, d.h, d.image)
					d.anim.default.frames = 1
					d.anim.default.duration = 1
				else
					for a,b in pairs(anims) do
						d.anim[a] = {}
						local x,y,aw,ah,dur = 0,0,0,0,0
						
						if (d.default == nil) then
							d.default = a
						end
						
						if (#b == 0) then
							x = 0
							y = 0
							aw = 1
							ah = 1
							dur = 1
						elseif (#b == 2) then
							x = b[1] * d.w
							y = b[2] * d.h
							aw = 1
							ah = 1
							dur = 1
						elseif (#b == 3) then
							x = 0
							y = 0
							aw = b[1]
							ah = b[2]
							dur = b[3] or 15
						else
							x = b[1] * d.w
							y = b[2] * d.h
							aw = b[3]
							ah = b[4]
							dur = b[5] or 15
						end
						
						local frames = aw * ah
						d.anim[a]["frames"] = frames
						d.anim[a]["duration"] = dur
						
						for f=0,frames-1 do
							local cx = x + (f % aw) * d.w
							local cy = y + math.floor(f / aw) * d.h
							
							table.insert(d.anim[a], love.graphics.newQuad(cx, cy, d.w, d.h, d.image))
						end
					end
				end
			end
		end,
		
		set = function(sprite,name,force_)
			local force = force_ or false
			
			if (sprite.anim ~= name) or force then
				if (sprite.data.anim[name] ~= nil) then
					sprite.anim = name
					sprite.timer = 0
					sprite.frame = 1
					sprite.update = true
					
					local data = spritesys.get(sprite,name)
					sprite.duration = data.duration or 15
					
					if (data.frames == 1) then
						sprite.update = false
					end
				else
					alert(name .. " anim doesn't exist")
				end
			end
		end,
		
		update = function()
			for i,unit in ipairs(UNITS) do
				if (unit.animation ~= nil) then
					unit.animation(unit)
				end
				
				if (unit.sprite ~= nil) and (unit.sprite.update ~= nil) and unit.sprite.update then
					local dur = unit.sprite.duration
					local name = unit.sprite.anim
					local data = spritesys.get(unit,name)
					
					if (data.frames > 1) then
						unit.sprite.timer = unit.sprite.timer + 1
						
						if (dur > 0) and (unit.sprite.timer >= dur) then
							unit.sprite.timer = 0
							unit.sprite.frame = unit.sprite.frame + 1
							if (unit.sprite.frame > data.frames) then
								unit.sprite.frame = 1
							end
						end
					end
				end
			end
		end,
	},
}

IMAGES = {}
SPRITES = {}
RENDER = {}
CANVAS = {}
ORDER = {}
SPRITELIST = {}