-- v.0.1.0
textsys =
{
	init = function(name)
		TEXT = {}
		IMGFONTS = {}
		
		for i,v in pairs(TEXTIMAGES) do
			textsys.setup(i,v)
		end
		
		if (name ~= nil) then
			IMGFONT = name
		end
	end,
	
	setup = function(name,data)
		IMGFONTS[name] = {}
		local d = IMGFONTS[name]
		if (#IMGFONT == 0) then
			IMGFONT = name
		end
		
		d.name = name
		d.file = data.file or name
		d.image = love.graphics.newImage(d.file ..".png")
		d.w = data.tilew or data.w or 5
		d.h = data.tileh or data.h or 8
		d.linew = math.floor(d.image:getWidth() / d.w)
		d.lookup = data.lookup
		d.letters = {}
		d.gap = data.gap or 1
		
		for i,l in ipairs(d.lookup) do
			local id = i
			local ldata = data.letters[l] or data.letters[id] or {}
			local w = ldata.w or data.w or d.w
			local h = ldata.h or data.h or d.h
			local lx = ldata.x or ((id-1) % d.linew) * d.w
			local ly = ldata.y or math.floor((id-1) / d.linew) * d.h
			
			d.letters[l] = {}
			local ld = d.letters[l]
			ld.l = l
			ld.w = w
			ld.h = h
			ld.id = id
			ld.image = love.graphics.newQuad(lx, ly, w, h, d.image)
		end
		
		if (data.letters ~= nil) then
			for l,v in pairs(data.letters) do
				if (d.letters[l] == nil) then
					d.letters[l] = {}
					local w = v.w or data.w or d.w
					local h = v.h or data.h or d.h
					local lx = v.x or 0
					local ly = v.y or 0
					
					local ld = d.letters[l]
					ld.l = l
					ld.w = w
					ld.h = h
					ld.id = id
					ld.image = love.graphics.newQuad(lx, ly, w, h, d.image)
				end
			end
		end
	end,
	
	write = function(text,ox,oy,id,data)
		local d = data or {}
		
		if (type(text) == "string") then
			local t = textsys.handle(text,d.font,d.wrap)
			local x = ox
			local y = oy
			
			if (d.owner ~= nil) then
				local unit = unitsys.get(d.owner)
				if (unit ~= nil) then
					t.owner = owner
					x = unit.xp + ox
					y = unit.yp + oy
				end
			end
			
			t.x = x
			t.y = y
			t.ox = ox
			t.oy = oy
			t.l = d.layer or 1
			t.id = id or "text"
			t.omx = d.xoffset or 0
			t.omy = d.yoffset or 0
			
			table.insert(TEXT, t)
		elseif (type(text) == "table") then
			local g = d.gap or 0
			local y = oy
			for i,v in ipairs(text) do
				local t = textsys.write(v,ox,y,id,data)
				y = y + t.h + g
			end
		end
		
		return TEXT[#TEXT]
	end,
	
	handle = function(text,font,wrap)
		local d = IMGFONTS[IMGFONT]
		local f = IMGFONT
		if (font ~= nil) then
			d = IMGFONTS[font]
			f = font
		end
		
		local result = {}
		local x = 0
		local y = 0
		local lh = d.h or 8
		local g = d.gap or 1
		local c = 0xffffff
		
		result.f = f
		result.lh = lh
		
		local i = 1
		while (i <= #text) do
			local l = string.sub(text, i, i)
			
			if (l ~= " ") and (l ~= "$") and (d.letters[l] ~= nil) then
				local letter = d.letters[l]
				local w = letter.w
				
				table.insert(result, {l, x, y, c})
				x = x + w + g
			elseif (l == "$") then
				local code = string.sub(text, i+1, i+1)
				local skip = 1
				
				if (code == "S") then
					x = 0
					y = y + lh + g
				elseif (code == "C") then
					local col = string.sub(text, i+2, i+7)
					skip = 7
					
					col = tonumber("0x" .. col)
					if (string.sub(text, i+2, i+2) == "W") then
						col = 0xffffff
						skip = 2
					end
					c = col
				else
					error("No $ command with code " .. code)
				end
				
				i = i + skip
			else
				local wrapped = false
				
				if (wrap ~= nil) then
					local x_ = 0
					local j = i
					for m=i,#text do
						l = string.sub(text, j, j)
						
						if (l == "$") then
							local code = string.sub(text, j+1, j+1)
							local code2 = string.sub(text, j+2, j+2)
							if (code == "C") and (code2 ~= "W") then
								j = j + 6
							end
						elseif (l ~= " ") then
							local letter = d.letters[l] or {}
							local w = letter.w or d.w
							x_ = x_ + w
						end
						
						if (((l == " ") and (j > i)) or (j >= #text)) then
							if (x + x_ >= wrap) then
								x = 0
								y = y + lh + g
								wrapped = true
							end
							
							break
						end
						
						j = j + 1
					end
				end
				
				if (wrapped == false) then
					x = x + d.w
				end
			end
			
			i = i + 1
		end
		
		y = y + lh
		
		result.w = x
		result.h = y
		
		return result
	end,
	
	drawthis = function(tdata)
		local x = tdata.x
		local y = tdata.y
		local fdata = IMGFONTS[tdata.f]
		local c = 0xffffff
		love.graphics.setColor(1, 1, 1, 1)
		
		for i,d in ipairs(tdata) do
			local id = d[1]
			local lx = d[2]
			local ly = d[3]
			local c_ = d[4] or 0xffffff
			
			if (c_ ~= c) then
				c = c_
				love.graphics.setColor(getrgb(c))
			end
			
			if (fdata.letters[id] ~= nil) then
				local rx = x + lx + tdata.w * tdata.omx
				local ry = y + ly + tdata.h * tdata.omy
				local ox = fdata.letters[id].w * 0.5
				local oy = fdata.letters[id].h * 0.5
				if integral then
					rx = math.floor(rx)
					ry = math.floor(ry)
					ox = math.floor(ox)
					oy = math.floor(oy)
				end
				rx = rx * scaling
				ry = ry * scaling
				
				love.graphics.draw(fdata.image, fdata.letters[id].image, rx, ry, 0, scaling, scaling, 0, 0)
			end
		end
	end,
	
	draw = function(l_)
		local l = l_ or 1
		
		for i,v in ipairs(TEXT) do
			if (v.l == l) then
				textsys.drawthis(v)
			end
		end
	end,
	
	erase = function(id)
		local these = {}
		
		for i,v in ipairs(TEXT) do
			if (id == nil) or (v.id == id) then
				table.insert(these, i)
			end
		end
		
		for i,v in ipairs(these) do
			table.remove(TEXT, v-(i-1))
		end
	end,
	
	font =
	{
		init = function(data)
			FONTS = {}
			
			for i,v in pairs(data) do
				FONTS[i] = {}
				local font = FONTS[i]
				local file = v[1]
				local size = v[2]
				
				font.font = love.graphics.newFont(file,size,"normal",2)
				font.file = file
				font.size = size
			end
		end,
		
		set = function(name)
			if (FONTS[name] ~= nil) then
				local f = FONTS[name]
				love.graphics.setFont(f.font)
				
				FONTS.current = f
			end
		end,
		
		size = function(name)
			if (name == nil) then
				if (FONTS.current ~= nil) then
					return FONTS.current.size
				end
			else
				if (FONTS[name] ~= nil) then
					return FONTS[name].size
				end
			end
		end,
		
		file = function(name)
			if (name == nil) then
				if (FONTS.current ~= nil) then
					return FONTS.current.file
				end
			else
				if (FONTS[name] ~= nil) then
					return FONTS[name].file
				end
			end
		end,
	},
}

TEXT = {}
IMGFONTS = {}
IMGFONT = ""
FONTS = {}
TEXTIMAGES = {}