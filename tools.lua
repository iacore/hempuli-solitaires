function update_pos()
	for i,unit in ipairs(UNITS) do
		local lerp = unit.lerp or 0.2
		unit.xp = unit.xp + (unit.x - unit.xp) * lerp
		unit.yp = unit.yp + (unit.y - unit.yp) * lerp
	end
end

function copytable(t)
	local result = {}
	
	for i,v in pairs(t) do
		if (type(v) ~= "table") then
			result[i] = v
		else
			result[i] = copytable(v)
		end
	end
	
	return result
end

function cardtopile(unit,pilename,pos)
	local pile = unitsys.find("pile",unit.pile)
	unit.pile = pilename
	unit.oldpile = pile.name
	
	for a,b in ipairs(pile.cards) do
		if (a > unit.pileid) then
			b.pileid = b.pileid - 1
		end
	end
	
	table.remove(pile.cards, unit.pileid)
	
	if (pos == nil) then
		table.insert(piles[pilename].cards, unit)
		unit.pileid = #piles[pilename].cards
		spritesys.zlayer.tofront(unit)
	else
		table.insert(piles[pilename].cards, pos, unit)
		unit.pileid = pos
		spritesys.zlayer.tofront(unit)
		
		for a,b in ipairs(piles[pilename].cards) do
			b.pileid = a
			
			if (a > pos) then
				spritesys.zlayer.tofront(b)
			end
		end
	end
	
	return pile
end

function getrgb(value,mult)
	local m = mult or 1.0
	local r = math.floor(value / (256 * 256)) % 256
	local g = math.floor(value / 256) % 256
	local b = value % 256
	
	r = math.min(r * m, 255)
	g = math.min(g * m, 255)
	b = math.min(b * m, 255)
	r = r / 255.0
	g = g / 255.0
	b = b / 255.0
	
	local result = {r,g,b}
	return result
end

function sortvalues(values)
	local result = {}
	for i,v in ipairs(values) do
		if (#result == 0) then
			table.insert(result, v)
		else
			local done = false
			for a,b in ipairs(result) do
				if (b > v) then
					table.insert(result, a, v)
					done = true
					break
				end
			end
			
			if (done == false) then
				table.insert(result, v)
			end
		end
	end
	
	return result
end

function sum(values)
	local result = 0
	for i,v in pairs(values) do
		result = result + v
	end
	return result
end

function max(values)
	local result
	for i,v in pairs(values) do
		if (result == nil) then
			result = v
		else
			result = math.max(result, v)
		end
	end
	return result
end

function min(values)
	local result
	for i,v in pairs(values) do
		if (result == nil) then
			result = v
		else
			result = math.min(result, v)
		end
	end
	return result
end