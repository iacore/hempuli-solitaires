-- v.0.1.0
gamesys =
{
	init = function()
		TITLE = GAME.title or "game"
		BACKCOL = GAME.colour_back or 0
		screenw = GAME.width or 640
		screenh = GAME.height or 480
		scaling = GAME.scaling or 1
		integral = GAME.scaling_int or false
		
		local filter = GAME.filter or "linear"
		local linestyle = GAME.linestyle or "smooth"
		
		love.window.setMode(screenw * scaling, screenh * scaling)
		love.graphics.setDefaultFilter(filter)
	end,
}

TITLE = ""
BACKCOL = 0
screenw = 640
screenh = 480
scaling = 1
integral = false

GAME = 
{
}