-- v.0.1.1
inputsys =
{
	input = function(key,data)
		if (INPUT[key] ~= nil) then
			INPUT[key](key,data)
		end
		
		if (INPUT_G[key] ~= nil) then
			INPUT_G[key](key,data)
		end
	end,
	
	set = function(inputdata)
		if (type(inputdata) == "table") then
			INPUT = inputdata
		elseif (type(inputdata) == "string") and (inputlist[inputdata] ~= nil) then
			INPUT = inputlist[inputdata]
		else
			INPUT = {}
		end
	end,
	
	setglobal = function(inputdata)
		if (type(inputdata) == "table") then
			INPUT_G = inputdata
		elseif (type(inputdata) == "string") and (inputlist[inputdata] ~= nil) then
			INPUT_G = inputlist[inputdata]
		else
			INPUT_G = {}
		end
	end,
	
	held = function(key)
		return love.keyboard.isDown(key)
	end,
	
	mouse_held = function(key)
		return love.mouse.isDown(key)
	end,
	
	mouse_isdown = function(button)
		if (inputsys.ignore == false) then
			if (button == 1) then
				inputsys.input("mouse_left_held")
			elseif (button == 2) then
				inputsys.input("mouse_right_held")
			elseif (button == 3) then
				inputsys.input("mouse_middle_held")
			end
			
			inputsys.input("mouse_held",button)
		end
	end,
	
	gethover = function(class,layer)
		local x,y = love.mouse.getPosition()
		local result = {}
		
		for i,unit in ipairs(UNITS) do
			local ux = unit.xp
			local uy = unit.yp
			local w,h = unit.sprite.w,unit.sprite.h
			local ox,oy = 0,0
			ox = unit.sprite.data.xoffset or 0
			oy = unit.sprite.data.yoffset or 0
			
			if (w > 0) and (h > 0) and ((class == nil) or (unit.class == class)) and ((layer == nil) or (unit.layer == layer)) then
				if (x >= (ux - w * 0.5 + ox) * scaling) and (y >= (uy - h * 0.5 + oy) * scaling) and (x < (ux + w * 0.5 + ox) * scaling) and (y < (uy + h * 0.5 + oy) * scaling) then
					table.insert(result, unit)
				end
			end
		end
		
		return result
	end,
}

inputsys.ignore = false
INPUT = {}
INPUT_G = {}

function love.keypressed(key)
	if (inputsys.ignore == false) then
		inputsys.input(key)
	end
end

function love.mousepressed(x,y,button,istouch)
	if (inputsys.ignore == false) then
		if (#BUTTONS > 0) and (delay == 0) then
			menusys.button.clickcheck(x,y,button)
		end
		
		if (button == 1) then
			inputsys.input("mouse_left",{x,y})
		elseif (button == 2) then
			inputsys.input("mouse_right",{x,y})
		elseif (button == 3) then
			inputsys.input("mouse_middle",{x,y})
		end
		
		inputsys.input("mouse",{x,y,button})
	end
end

function love.wheelmoved(wx, wy)
	if (inputsys.ignore == false) then
		inputsys.input("mouse_wheel",wy)
	end
end