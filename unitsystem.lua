unitsys =
{
	create = function(class,name,x,y,dat)
		table.insert(UNITS, {})
		local unit = UNITS[#UNITS]
		
		local data = dat or {}
		unit.class = class or data._class
		local unitdata = data._unitdata or unit.class
		local udata = UNITLIST[unitdata] or UNITLIST[unit.class] or {}
		
		unit.name = name or udata._name or data._name or unit.class
		unit.originx = x
		unit.originy = y
		unit.x = x
		unit.y = y
		unit.xp,unit.yp = x,y
		unit.id = data._id or unitsys.newid()
		unit.dir = 0
		unit.w = 0
		unit.h = 0
		unit.zlayer = 999
		unit.layer = 1
		
		for i,v in pairs(udata) do
			if (string.sub(i, 1, 1) ~= "_") then
				if (type(v) ~= "table") then
					unit[i] = v
				else
					unit[i] = copytable(v)
				end
			end
		end
		
		for i,v in pairs(data) do
			if (string.sub(i, 1, 1) ~= "_") then
				if (type(v) ~= "table") then
					unit[i] = v
				else
					unit[i] = copytable(v)
				end
			end
		end
		
		local hassprite = false
		
		if (data._sprites ~= nil) or (udata._sprites ~= nil) then
			local sd = data._sprites or udata._sprites
			unit.sprites = {}
			local anim --= udata._anim or data._anim
			local visible = true
			if (data._visible ~= nil) then
				visible = data._visible
			elseif (udata._visible ~= nil) then
				visible = udata._visible
			end
			
			for i,v in ipairs(sd) do
				local sprite = v
				unit.sprites[i] = spritesys.set(unit,sprite,anim,visible)
			end
			
			hassprite = true
		end
		
		if (data._sprite ~= nil) or (udata._sprite ~= nil) or (SPRITES[unit.class] ~= nil) then
			local sprite = udata._sprite or data._sprite or unit.class
			local anim = udata._anim or data._anim
			local visible = true
			if (data._visible ~= nil) then
				visible = data._visible
			elseif (udata._visible ~= nil) then
				visible = udata._visible
			end
			
			unit.sprite = spritesys.set(unit,sprite,anim,visible)
			hassprite = true
		end
		
		if hassprite then
			spritesys.zlayer.add(unit)
		end
		
		return unit
	end,
	
	remove = function(id)
		local u = unitsys.get(id)
		
		if (u ~= nil) and (u.sprite ~= nil) then
			spritesys.zlayer.remove(u)
		end
		
		for i,unit in ipairs(UNITS) do
			if (unit.id == id) then
				table.remove(UNITS, i)
			end
		end
	end,
	
	remove_byclass = function(class,name)
		local del = {}
		for i,unit in ipairs(UNITS) do
			if ((class == nil) or (class == unit.class)) and ((name == nil) or (name == unit.name)) then
				table.insert(del, unit.id)
			end
		end
		
		for i,v in ipairs(del) do
			unitsys.remove(v)
		end
	end,
	
	newid = function()
		UNIT_ID = UNIT_ID + 1
		return UNIT_ID
	end,
	
	reset = function()
		UNITS = {}
		UNIT_ID = 0
		ORDER = {}
	end,
	
	get = function(id)
		for i,unit in ipairs(UNITS) do
			if (unit.id == id) then
				return unit
			end
		end
		
		return nil
	end,
	
	find = function(class,name)
		for i,unit in ipairs(UNITS) do
			if ((class == nil) or (unit.class == class)) and ((name == nil) or (unit.name == name)) then
				return unit
			end
		end
		
		return nil
	end,
}

UNITS = {}
UNIT_ID = 0
UNITLIST = {}