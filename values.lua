GAME =
{
	title = "Solitaire",
	width = 568,
	height = 380,
	colour_back = 0,
	scaling = 2.0,
	scaling_int = true,
	filter = "nearest",
	linestyle = "rough",
}

SPRITELIST =
{
	card =
	{
		file = "gfx/deck",
		w = 48,
		h = 80,
		default = "back",
		anim =
		{
			back = {0,4},
			pile_light = {1,4},
			pile_dark = {2,4},
			pile = {3,4},
			pile_white = {4,4},
			
			a1 = {0,0},
			a2 = {1,0},
			a3 = {2,0},
			a4 = {3,0},
			a5 = {4,0},
			a6 = {5,0},
			a7 = {6,0},
			a8 = {7,0},
			a9 = {8,0},
			a10 = {9,0},
			a11 = {10,0},
			a12 = {11,0},
			a13 = {12,0},
			
			b1 = {0,1},
			b2 = {1,1},
			b3 = {2,1},
			b4 = {3,1},
			b5 = {4,1},
			b6 = {5,1},
			b7 = {6,1},
			b8 = {7,1},
			b9 = {8,1},
			b10 = {9,1},
			b11 = {10,1},
			b12 = {11,1},
			b13 = {12,1},
			
			c1 = {0,2},
			c2 = {1,2},
			c3 = {2,2},
			c4 = {3,2},
			c5 = {4,2},
			c6 = {5,2},
			c7 = {6,2},
			c8 = {7,2},
			c9 = {8,2},
			c10 = {9,2},
			c11 = {10,2},
			c12 = {11,2},
			c13 = {12,2},
			
			d1 = {0,3},
			d2 = {1,3},
			d3 = {2,3},
			d4 = {3,3},
			d5 = {4,3},
			d6 = {5,3},
			d7 = {6,3},
			d8 = {7,3},
			d9 = {8,3},
			d10 = {9,3},
			d11 = {10,3},
			d12 = {11,3},
			d13 = {12,3},
		},
	},
	pile =
	{
		file = "gfx/pile",
		w = 48,
		h = 80,
		default = "pile",
		anim =
		{
			pile_light = {0,0},
			pile_dark = {1,0},
			pile = {2,0},
			pile_white = {3,0},
			pile_a = {0,1},
			pile_b = {1,1},
			pile_c = {2,1},
			pile_d = {3,1},
		},
	},
	icon =
	{
		file = "gfx/icons",
		w = 48,
		h = 80,
		default = "thing",
		anim =
		{
			thing = {0,0},
			babaex = {1,0},
			babataire = {2,0},
			eldritch = {3,0},
			cheat = {4,0},
			wolf = {5,0},
			poker = {6,0},
			lock = {7,0},
			onestack = {8,0},
			onesuit = {9,0},
			code = {10,0},
			big = {11,0},
			alchemy = {12,0},
		},
	},
	thumbnail =
	{
		file = "gfx/games",
		w = 284,
		h = 192,
		default = "start",
		anim =
		{
			start = {0,0},
			babataire = {1,0},
			babaex = {2,0},
			wolf = {0,1},
			cheat = {1,1},
			eldritch = {2,1},
			lock = {0,2},
			poker = {1,2},
			thing = {2,2},
		},
	},
	button =
	{
		file = "gfx/buttons",
		w = 18,
		h = 18,
		default = "close",
		anim =
		{
			close = {0,0},
			restart = {1,0},
			info = {2,0},
			stack = {3,0},
		},
	},
}

UNITLIST =
{
	card =
	{
		pile = "deck",
		pileid = 0,
		open = true,
		open_place = false,
		open_pick = false,
		suit = 0,
		value = 0,
		oldpile = "",
	},
	pile =
	{
		cards = {},
		open = true,
		card_offsets = {0,0},
		homesuit = 0,
	},
}

SCRIPTS = {}
VARS = 
{
	GLOBAL =
	{
		mode = 0,
		mode_timer = 0,
		reveal_timer = 0,
		victory = 0,
	},
}
SCRIPT_MODS = {"babataire","thing","eldritch","cheat","wolf","lock","babaex","poker"}

subgames =
{
	babataire =
	{
		title = "TAP SOLITAIRE",
		title_colour = "e6a770",
		desc = "A fairly traditional solitaire. The twist is that you can place up to 3 cards sideways to start new temporary stacks in order to manage things more easily.$SOriginally called Babataire, this solitaire features cute Baba Is You -themed cards.",
		rating = "***--",
		deck = "Yes",
	},
	babaex =
	{
		title = "BABATAIRE",
		title_colour = "e37269",
		back_colour = 0x87265a,
		desc = "Built from Tap Solitaire with the intention of trying to create a solitaire with Baba Is You -esque rule-changing gameplay. The result is pretty chaotic but maybe that's appropriate?$SOriginally called Babataire EX, because the name 'Babataire' had been taken by what is now Tap Solitaire.",
		rating = "****-",
		deck = "Yes; but remembering the exact meanings of the face cards is probably annoying",
	},
	thing =
	{
		title = "UNEVEN SOLITAIRE",
		title_colour = "8faf62",
		back_colour = 0x523432,
		desc = "Another quite traditional solitaire. The 'uneven' part of the name comes from the fact that the rules for placement and picking of stacks are different, and thus you need to be careful not to lock yourself out of valid moves.",
		rating = "***--",
		deck = "Yes",
	},
	eldritch =
	{
		title = "ELDRITCH INVASION",
		title_colour = "c56f68",
		back_colour = 0x6a4d72,
		desc = "Possibly the most themed of the bunch. Defeat 16 eldritch monsters by matching them up with appropriate cards! Features a special deck with more esoteric suits: Moons, Knives, Books and Candles.",
		rating = "**---",
		deck = "Yes",
	},
	cheat =
	{
		title = "CHEATDECK SOLITAIRE",
		title_colour = "e37269",
		back_colour = 0x4276a9,
		desc = "Inspired in part by a magician's deck of cards seen years ago, which featured 52 copies of the 7 of diamonds. There are only 2 types of cards, and their 'values' change based on stack size.",
		rating = "**---",
		deck = "Yes; shuffle together 2 decks with one being face-up, one face-down",
	},
	wolf =
	{
		title = "COUNCIL OF SECRETS",
		title_colour = "e3c188",
		back_colour = 0x8c4444,
		desc = "Another slightly more themed solitaire, inspired by the game 'Werewolf'. Solve a traditional solitaire while also trying to identify 12 face cards based on their behaviour!",
		rating = "***--",
		deck = "$Cffa32aNo; the hidden information can't really be recreated in single-player format",
	},
	lock =
	{
		title = "LOCK SOLITAIRE",
		title_colour = "57a3c9",
		back_colour = 0x4f5779,
		desc = "A simple variation of a traditional solitaire setup. Every time you move a card, it gets locked in place for 2 moves. Thus, you'll need to consider your resources carefully so as not to get stuck.",
		rating = "**---",
		deck = "Yes",
	},
	poker =
	{
		title = "ROYAL FLUSH SOLITAIRE",
		title_colour = "c18962",
		back_colour = 0x523432,
		desc = "A funky take of solitaire in which your goal is to try to construct poker hands. Each hand gives you points based on the cards, and you must consider your options very carefully.",
		rating = "****-",
		deck = "Yes",
	},
}

TEXTIMAGES = 
{
	default =
	{
		file = "font",
		tilew = 5,
		tileh = 8,
		w = 3,
		h = 8,
		lookup = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9",".",",","?","!","-","'","_","/",":",";","(",")",'"',},
		letters =
		{
			A = {w = 4},
			B = {w = 4},
			C = {w = 4},
			D = {w = 4},
			E = {w = 4},
			F = {w = 4},
			G = {w = 4},
			H = {w = 4},
			I = {w = 3},
			J = {w = 4},
			K = {w = 4},
			L = {w = 4},
			M = {w = 5},
			N = {w = 4},
			O = {w = 4},
			P = {w = 4},
			Q = {w = 4},
			R = {w = 4},
			S = {w = 4},
			T = {w = 5},
			U = {w = 4},
			V = {w = 4},
			W = {w = 5},
			X = {w = 5},
			Y = {w = 5},
			Z = {w = 4},
			i = {w = 1},
			l = {w = 1},
			m = {w = 5},
			w = {w = 5},
			["0"] = {w = 4},
			["1"] = {w = 4},
			["2"] = {w = 4},
			["3"] = {w = 4},
			["4"] = {w = 4},
			["5"] = {w = 4},
			["6"] = {w = 4},
			["7"] = {w = 4},
			["8"] = {w = 4},
			["9"] = {w = 4},
			["."] = {w = 1},
			[","] = {w = 2},
			["?"] = {w = 4},
			["!"] = {w = 1},
			["'"] = {w = 1},
			[":"] = {w = 1},
			[";"] = {w = 2},
			["("] = {w = 2},
			[")"] = {w = 2},
		}
	},
	header =
	{
		file = "font_big",
		tilew = 6,
		tileh = 12,
		w = 5,
		h = 11,
		lookup = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","0","1","2","3","4","5","6","7","8","9",".",",","!","?",":",";",")","(","/","'",'"',"-","_","#","%","*","¨","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","+"},
		letters =
		{
			I = {w = 3},
			["1"] = {w = 3},
			["."] = {w = 1},
			[","] = {w = 2},
			["!"] = {w = 1},
			["'"] = {w = 1},
			[":"] = {w = 1},
			[";"] = {w = 2},
			["("] = {w = 3},
			[")"] = {w = 3},
			["-"] = {w = 3},
			['"'] = {w = 3},
			["f"] = {w = 4},
			["g"] = {h = 12},
			["i"] = {w = 1},
			["j"] = {w = 3, h = 12},
			["l"] = {w = 1},
			["p"] = {h = 12},
			["q"] = {h = 12},
			["r"] = {w = 4},
			["y"] = {h = 12},
		}
	},
	serif =
	{
		file = "font_serif",
		tilew = 15,
		tileh = 23,
		w = 14,
		h = 22,
		lookup = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"},
		letters =
		{
			["A"] = {w = 15},
			["C"] = {w = 13},
			["H"] = {w = 15},
			["I"] = {w = 6},
			["J"] = {w = 12},
			["K"] = {w = 15},
			["L"] = {w = 15},
			["M"] = {w = 15},
			["N"] = {w = 15},
			["O"] = {w = 13},
			["P"] = {w = 13},
			["Q"] = {w = 13},
			["R"] = {w = 15},
			["S"] = {w = 13},
			["U"] = {w = 15},
			["V"] = {w = 15},
			["W"] = {w = 15},
		}
	},
}